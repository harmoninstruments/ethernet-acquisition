# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import sys
sys.path.append("harmon-instruments-open-hdl")
import time
from amaranth import *

from XC7.XADC import XADC
from XC7.DNA import DNA
from XC7.SERDES import *
from XC7.clockbuffers import bufr_instance, bufio_instance, bufg_instance
from XC7.STARTUPE2 import STARTUPE2
from Ethernet.XC7_RGMII import Ethernet_RGMII
from RiscV.RiscV import RiscV
from SPI.flash import SPI_flash_XC7

class ClockGen(Elaboratable):
    def __init__(self):
        self.idelayready = Signal()
        self.pll_locked = Signal()
        self.reset = Signal(reset=1)
    def elaborate(self, platform):
        clk125_pin = platform.request("clk125")#, xdr='-')
        m = Module()
        m.domains.sync = ClockDomain(reset_less=True) # 125 MHz via BUFG
        m.domains.sync_90 = ClockDomain(reset_less=True) # 125 MHz via BUFIO at 90 degrees
        m.domains.sync_io = ClockDomain(reset_less=True) # 125 MHz via BUFIO
        m.domains.clk200 = ClockDomain(reset_less=True)
        clkout = Signal(7)
        clkoutb = Signal(4)
        ports = {}
        for i in range(len(clkout)):
            ports["o_CLKOUT{}".format(i)] = clkout[i]
        for i in range(len(clkoutb)):
            ports["o_CLKOUT{}B".format(i)] = clkoutb[i]

        pll_fb = Signal()
        pll_fbb = Signal()

        m.submodules.mmcm_sys = Instance(
            "MMCME2_BASE",
            p_STARTUP_WAIT="TRUE", o_LOCKED=self.pll_locked,
            # VCO @ 1GHz
            p_REF_JITTER1=0.01, p_CLKIN1_PERIOD=8.0,
            p_CLKFBOUT_MULT_F=8, p_DIVCLK_DIVIDE=1,
            i_CLKIN1=clk125_pin, i_CLKFBIN=pll_fb, o_CLKFBOUT=pll_fb,
            i_RST=C(0),
            i_PWRDWN=C(0),
            # outputs
            o_CLKFBOUTB=pll_fbb,
            p_CLKOUT0_DIVIDE_F=2, p_CLKOUT0_PHASE=0.0,
            p_CLKOUT1_DIVIDE=4, p_CLKOUT1_PHASE=0.0,
            p_CLKOUT2_DIVIDE=8, p_CLKOUT2_PHASE=0.0,
            p_CLKOUT3_DIVIDE=8, p_CLKOUT3_PHASE=270.0,
            p_CLKOUT4_DIVIDE=5, p_CLKOUT4_PHASE=0.0,
            **ports,
        )
        #m.submodules.bufr_sys = bufr_instance(div=1, i=clkout[2], o="sysr")
        #m.submodules.bufio_sys4x = bufio_instance(i=clkout[0], o="sys4x")
        #m.submodules.bufg_250 = bufg_instance(i=clkout[1], o="sys2x")
        m.submodules.bufg_125 = bufg_instance(i=clkout[2], o="sync")
        m.submodules.bufio_125 = bufio_instance(i=clkout[2], o="sync_io")
        m.submodules.bufio_125_90 = bufio_instance(i=clkout[3], o="sync_90")
        m.submodules.bufg_200 = bufg_instance(i=clkout[4], o="clk200")

        reset_counter = Signal(8, reset=255)
        with m.If(reset_counter != 0):
            m.d.sync += reset_counter.eq(reset_counter - 1)
        m.d.sync +=  self.reset.eq(reset_counter != 0)

        m.submodules.idelayctrl = idelayctrl_instance(clk="clk200", reset=self.reset,
                                                      ready=self.idelayready)
        return m

class EtherAcq(Elaboratable):
    def elaborate(self, platform):
        platform.create_pblock("X1Y1", "X1Y1")
        m = Module()
        clockgen = m.submodules.clockgen = ClockGen()
        count = Signal(32)
        m.d.sync += count.eq(count + 1)

        rdata_rx = Signal(32)
        cpu = RiscV(reset=clockgen.reset, programfile='../firmware/firmware.bin', rdata_or=rdata_rx)
        cpu.add_ro_reg(0x10000000, count)
        cpu.add_ro_reg(0x10000004, int(time.time()))
        led = platform.request("led", 0)
        cpu.add_wo_reg(0x10000004, led)
        cpu.add_ro_reg(0x10000008, Cat(clockgen.idelayready, clockgen.pll_locked))
        startup = m.submodules.startup = STARTUPE2()
        m.submodules.flash = SPI_flash_XC7(
            pins=platform.request('flash'), sck=startup.usrcclk, pio=cpu, addr=0x10000010)
        m.submodules.dna = DNA(cpu, 0x1000000C)
        m.submodules.xadc = XADC(cpu, 0x10000100)

        # 0x20000000 - 0x3FFFFFFF
        m.submodules.ethernet = Ethernet_RGMII(n=0, cpu=cpu, count=count, rdata=rdata_rx)

        platform.add_to_pblock("X1Y1", "ethernet/rxbuf/fifo/fifo_inst")
        platform.add_to_pblock("X1Y1", "ethernet/rxbuf/fifo")
        platform.add_to_pblock("X1Y1", "cpu")

        m.submodules.cpu = cpu

        return m
