#!/usr/bin/env python3
# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: MIT

from amaranth import *
from enum import Enum, unique
import sys
sys.path.append("minerva/")
from minerva import core
import numpy as np
#from AXI import AXIStream

@unique
class TXState(Enum):
    INTERFRAME = 0
    PREAMBLE = 1
    DATA = 2
    CRC = 3

"""
wishbone_layout = [
    ("adr",   30, DIR_FANOUT),
    ("dat_w", 32, DIR_FANOUT),
    ("dat_r", 32, DIR_FANIN),
    ("sel",    4, DIR_FANOUT),
    ("cyc",    1, DIR_FANOUT),
    ("stb",    1, DIR_FANOUT),
    ("ack",    1, DIR_FANIN),
    ("we",     1, DIR_FANOUT),
    ("cti",    3, DIR_FANOUT), what
    ("bte",    2, DIR_FANOUT), what
    ("err",    1, DIR_FANIN)
]"""

class SOC(Elaboratable):
    def __init__(self):
        self.txd = Signal(8)
    def elaborate(self, platform):
        m = Module()
        prog = np.fromfile("sw/firmware.bin", dtype=np.uint32)
        dram = Memory(width=32, depth=1024, init=prog)
        iram_rdport = m.submodules.iram_rdport = dram.read_port(domain="sync", transparent=False)
        dram_rdport = m.submodules.dram_rdport = dram.read_port(domain="sync", transparent=False)
        dram_wrport = m.submodules.dram_wrport = dram.write_port(domain="sync", granularity=8)
        soc = m.submodules.minerva = core.Minerva()
        m.d.comb += iram_rdport.addr.eq(soc.ibus.adr[:10])
        m.d.comb += soc.ibus.dat_r.eq(iram_rdport.data)
        m.d.sync += soc.ibus.ack.eq(soc.ibus.stb)

        m.d.comb += dram_rdport.addr.eq(soc.dbus.adr[:10])
        m.d.comb += soc.dbus.dat_r.eq(dram_rdport.data)
        stb_del = Signal()
        m.d.sync += stb_del.eq(soc.dbus.stb)
        m.d.comb += soc.dbus.ack.eq(Mux(soc.dbus.we, soc.dbus.stb, stb_del))
        m.d.comb += dram_wrport.addr.eq(soc.dbus.adr[:10])
        m.d.comb += dram_wrport.data.eq(soc.dbus.dat_w)
        m.d.comb += dram_wrport.en.eq(Mux(soc.dbus.stb & soc.dbus.we & (soc.dbus.adr[10:] == 0), soc.dbus.sel, 0))
#        with m.Switch(state):
#            with m.Case(TXState.INTERFRAME):
        return m

if __name__ == '__main__':
    from amaranth.back.pysim import Simulator, Passive
    #stream = AXIStream('txdata', u=[('last', 1), ('data', 8)])
    dut = SOC()
    import queue
    txq = queue.Queue()
    def send_packet(packet):
        for byte in packet[:-1]:
            txq.put({'data':byte, 'last':0})
        txq.put({'data':packet[-1], 'last':1})

    def listener():
        yield Passive()
        # process a packet
        while True:
            data = bytearray()
            # wait for the first byte
            interframe = 0
            while True:
                dv = yield dut.txc
                byte = yield dut.txd
                yield
                if dv == RGMII_Status.IDLE.value:
                    interframe += 1
                    if len(data) != 0:
                        break
                    continue
                data.append(byte)
            print(len(data), data == packet_sof_crc, interframe, data, '\n')
            assert interframe >= 12
            assert data == packet_sof_crc

    def test_proc():
        #yield from send_packet(packet, err=True)
        for i in range(1200):
            yield
    sim = Simulator(dut, vcd_file=open("dump.vcd", "w"))
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    #sim.add_sync_process(listener)
    sim.run()
