# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth.build import *
from amaranth.vendor.xilinx_7series import *

attrs_18_slow = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4")
attrs_18_slow_pu = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4", PULLUP="TRUE")
hstl_18_pu = Attrs(IOSTANDARD="HSTL_I_18", PULLUP="TRUE")
hstl_18 = Attrs(IOSTANDARD="HSTL_I_18")

class EtherAcqPlatform(Xilinx7SeriesPlatform):
    device      = "xc7a15t"
    package     = "ftg256"
    speed       = "1"
    default_clk = "sync"
    resources = [
        Resource("clk125", 0, Pins("D4", dir="i"), Clock(125e6), attrs_18_slow),
        Resource("led", 0, Pins("N1", dir="o"), attrs_18_slow),
        Resource("ufl", 0, Pins("F4", dir="o"), hstl_18),
        Resource("ufl", 1, Pins("J16", dir="o"), hstl_18),
        Resource("gbeth", 0,
                 Subsignal("txclk", Pins("K2", dir="o"), hstl_18),
                 Subsignal("txd", Pins("K3 L2 L3 K1", dir="o"), hstl_18),
                 Subsignal("txen", Pins("J1", dir="o"), hstl_18),
                 Subsignal("rxclk", Pins("E3", dir="i"), Clock(125e6), hstl_18_pu),
                 Subsignal("rxd", Pins("G1 H2 H1 J3", dir="i"), hstl_18_pu),
                 Subsignal("rxdv", Pins("G2", dir="i"), hstl_18_pu),
                 Subsignal("mdc", Pins("E1", dir="o"), attrs_18_slow),
                 Subsignal("mdio", Pins("E2", dir="io"), attrs_18_slow_pu),
                 Subsignal("resetn", Pins("D1", dir="o"), attrs_18_slow),
                 Subsignal("clock", Pins("C1", dir="o"), hstl_18), # 25 MHz out to PHY
        ),
        Resource("flash", 0,
                 Subsignal("cs", Pins("L12", dir="o")),
                 Subsignal("cipo", Pins("J14", dir="i")),
                 Subsignal("copi", Pins("J13", dir="o")),
                 attrs_18_slow_pu,
        ),

    ]
    connectors = [
        Connector("exp", 0, "A12 B11 C11 A10 B10 A9 B9 F5 A7 B7 B6 A5 B5 A4 B4 A3 C3 A3 B1 B1"),
    ]

    constraints = ""

    def create_pblock(self, name, region):
        self.constraints += "create_pblock {pblock}\nresize_pblock [get_pblocks {pblock}] -add {{CLOCKREGION_{cr}:CLOCKREGION_{cr}}}\n".format(pblock="pblock_"+name, cr=region)

    def add_to_pblock(self, name, cells):
        self.constraints += "add_cells_to_pblock [get_pblocks {pblock}] [get_cells -quiet [list {cells}]]\n".format(pblock="pblock_"+name, cells=cells)

    def toolchain_prepare(self, fragment, name, **kwargs):
        overrides = {
            #"set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 2 [current_design]",
            #"script_after_bitstream":
            #    "write_cfgmem -force -format bin -interface spix4 -size 16 "
            # "-loadbit \"up 0x0 {name}.bit\" -file {name}.bin".format(name=name),
            "add_constraints": "set_property INTERNAL_VREF 0.9 [get_iobanks 35]\n"
            #"set_property INTERNAL_VREF 0.9 [get_iobanks 35]\n"
            "set_property CONFIG_VOLTAGE 1.8 [current_design]\n"
            "set_property CFGBVS GND [current_design]\n"
            "set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]\n"
            "set_operating_conditions -ambient_temp 55.0\n" + self.constraints,
            "script_after_synth": r"""
            foreach cell [get_cells -quiet -hier -filter {hi.false_path != ""}] {
                set_false_path -from [get_property hi.false_path $cell] -to $cell
            }
            """
        }
        return super().toolchain_prepare(fragment, name, **overrides, **kwargs)

if __name__ == "__main__":
    from artix import EtherAcq
    EtherAcqPlatform().build(EtherAcq(), do_program=False)
