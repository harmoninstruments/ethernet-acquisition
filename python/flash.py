#!/usr/bin/env python3

import struct, time, sys, os, socket
from timeit import default_timer as timer

UDP_IP = "10.42.0.2"
SPI_PORT = 42003
spisock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
spisock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
spisock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
spisock.bind(("", SPI_PORT))

def addr24(addr):
    return struct.pack(">I", addr)[1:]

def flash_spi(d):
    spisock.sendto(d, (UDP_IP, SPI_PORT))
    data, addr = spisock.recvfrom(1400)
    return data

def flash_wren():
    flash_spi(bytes((0x06,)))

def flash_read_single(addr, count):
    r = flash_spi(bytes((0x0B,)) + addr24(addr) + bytes(count+1))
    return r[-count:]

def flash_read(addr, count):
    rv = bytearray(0)
    while count:
        chunk = min(1024, count)
        rv += flash_read_single(addr, chunk)
        count -= chunk
        addr += chunk
    return rv

def flash_read_id():
    a = flash_spi(bytes((0x9F,)) + bytearray(100))
    return a[1:]

def flash_read_status():
    a = flash_spi(bytes((0x05,0)))
    return a[1]

def flash_wait_busy():
    start = timer()
    while True:
        status = flash_read_status()
        if (status & 0x01) == 0:
            return
        if (timer() - start) > 1.0:
            print("timeout")
            exit(-1)

def flash_write_status(val):
    flash_spi(bytes((0xBB,val)))
    flash_wait_busy()

def flash_page_program(addr, data):
    assert (addr & 0xFF) == 0
    assert len(data) == 256
    flash_wren()
    flash_spi(bytes((0x02,)) + addr24(addr) + data)
    flash_wait_busy()

def flash_sector_erase(addr):
    print("erasing", addr)
    assert (addr & 0xFF00FFFF) == 0
    flash_wren()
    flash_spi(bytes((0xD8,)) + addr24(addr))
    flash_wait_busy()

def flash_sector_write(addr, data):
    flash_write_status(0)
    flash_sector_erase(addr)
    if len(data) < 65536:
        data += bytes(65536-len(data))
    assert len(data) <= 65536
    for i in range(256):
        flash_page_program(addr + 256*i, data[256*i:256*(i+1)])
    rb = flash_read(addr, 65536)
    assert data == rb

def flash_write(addr, data):
    for i in range((len(data)+65535)//65536):
        flash_sector_write(addr + 65536*i, data[65536*i:65536*(i+1)])

def flash_write_file(addr, fn, bitreverse=False):
    with open(fn, "rb") as f:
        d = f.read()
        if bitreverse:
            d = bytearray(d)
            for i in range(len(d)):
                d[i] = int('{:08b}'.format(d[i])[::-1], 2)
        flash_write(addr, d)

if __name__ == "__main__":
    flash_read_id()
    print("SR", flash_read_status())
    print(flash_read_id())
    flash_write_file(0, sys.argv[1])
