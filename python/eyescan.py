#!/usr/bin/env python3

import struct, time, sys, os, socket
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from memory import mem_read, mem_write

drp_addr = 0x10000014

def drp_access(addr, data=0, w=0):
    assert addr < 2**9
    mem_write(drp_addr, data | (addr << 16) | (w<<25))
    for i in range(10):#while True:
        rv = mem_read(drp_addr)
        #print(hex(addr), hex(rv))
        if (rv & 0x10000) == 0:
            return rv

def drp_read(addr):
    return drp_access(addr)

def drp_write(addr, data):
    return drp_access(addr, data, w=1)

# ES_QUAL_MASK = 80 1's
for i in range(0x31, 0x36):
    drp_write(i, 0xFFFF)
# ES_SDATA_MASK = {40{1'b1}}, {32{1'b0}}, {8{1'b1}}
drp_write(0x36, 0x00FF)
drp_write(0x37, 0x0000)
drp_write(0x38, 0xFF00)
drp_write(0x39, 0xFFFF)
drp_write(0x3A, 0xFFFF)

def measure(x, y, prescale, ut):
    ES_PRESCALE = prescale
    # ES_CONTROL = 0, ERRDET_EN = 1, SCAN_EN = 1
    drp_write(0x3D, (3<<8))
    ysign = 1 if y < 0 else 0
    if (y < 0):
        y = -y
    ES_VERT_OFFSET = (y & 0x7F) | (ysign << 7) | (ut << 8)
    drp_write(0x3B, (ES_PRESCALE << 11) | (ES_VERT_OFFSET))
    ES_HORZ_OFFSET = x & 0x7FF
    drp_write(0x3C, ES_HORZ_OFFSET)
    drp_write(0x3D, (3<<8) | 1)
    while True:
        es_control_status = drp_read(0x151)
        #print("status = ", es_control_status)
        state = es_control_status >> 1
        if state == 2: # END
            break
    error_count = drp_read(0x14F)
    sample_count = drp_read(0x150)
    print(x,y,sample_count, error_count)
    # this should be 1+prescale, but 4+prescale gives correct
    # 0.5 BER in corners
    return error_count / (sample_count * 2**(4+prescale))

def measure_dfe(x,y, prescale):
    a = measure(x,y,prescale,0)
    b = measure(x,y,prescale,1)
    return (a+b)/2

for i in range(0x0, 0x40):
    print(hex(i), hex(drp_read(i)))

a = np.zeros((63, 65))

prescale = 7

for x in range(-32,33):
    for y in range(-31,32):
        err = measure_dfe(x,y*4,prescale=prescale)
        print(x, y, err)
        a[y+31, x+32] = (err + 1e-12)

print(a)
plt.imshow(a, cmap='viridis', interpolation='nearest', norm=LogNorm(), extent=[-0.5,0.5,-0.5,0.5])
plt.colorbar()
plt.xlabel("time offset (UI)")
plt.ylabel("amplitude offset (arb)")
plt.show()
