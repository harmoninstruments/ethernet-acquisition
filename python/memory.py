#!/usr/bin/env python3

import struct, socket

UDP_IP = "10.42.0.2"
MEM_PORT = 42002
memsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
memsock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
memsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
memsock.bind(("", MEM_PORT))

def mem_read(addr):
    memsock.sendto(struct.pack("I", addr | 2), (UDP_IP, MEM_PORT))
    data, addr = memsock.recvfrom(1024)
    (a, v,) = struct.unpack("II", data[:8])
    return v

def mem_write(addr, data, recv=True):
    memsock.sendto(struct.pack("II", addr | 1, data), (UDP_IP, MEM_PORT))
    if recv:
        data, addr = memsock.recvfrom(1024)
