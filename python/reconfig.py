#!/usr/bin/env python3

import struct, time, sys, os, socket
from memory import mem_read, mem_write

port_addr = 0x10000020

def write_icap(data, recv=True):
    mem_write(port_addr, data, recv=recv)

flashaddr=0

write_icap(0xFFFFFFFF) # dummy
write_icap(0xAA995566) # sync
write_icap(0x20000000) # nop
write_icap(0x30020001) # WBSTAR command
write_icap(flashaddr)  # warm boot start addr
write_icap(0x30008001) # write CMD
write_icap(0x0000000F, recv=False) # IPROG
write_icap(0x20000000, recv=False) # nop
