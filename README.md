# ethernet-acquisition

## Small
![PCB photo](images/small.jpg)
 - Xilinx Artix 7 FPGA, option of XC7A15T - XC7A100T in FT256 package
 - 10/100/1000 Ethernet with Microchip KSZ9031 PHY
 - 125 MHz oscillator
 - 4 expansion connectors (details below)
 - SPI configuration flash - 128 Mib
 - designed with Kicad
 - 4 layer PCB, OSHPark compatible
 - 67 x 38 mm
 - 5 V power input on JST PA connector
 - status: 1 prototype built, functioning

## Medium
![PCB photo](images/medium_render.png)
 - Xilinx Kintex 7 FPGA, XC7K160T-2FFG676E
 - 2x QFSP+ cage, each supports 4x 10G or 1x 40G Ethernet, other high speed serial standards possible
 - 156.25 MHz VCXO with tuning voltage control from FPGA
 - 5 expansion connectors with 20 1.8 V IO each, 1.8 and 3.7 V provided. 3 are HP IO and support LVDS, 2 HR. One HR can be configured to use an external VCCIO of 1.2 to 3.3 V.
 - SPI configuration flash - 128 Mib
 - designed with Kicad
 - 6 layer controlled impedance PCB
 - 90 x 65 mm
 - 9 - 15 V power input on JST PA connector
 - status: 3 prototypes built, functioning, used in phase noise measurement setup

## FFVB676
 - Xilinx Artix or Kintex Ultrascale+ FPGA in FFVB676 package
 - XCUA10P to XCKU5P which is the largest part free Vivado supports
 - 2 QFSP+ cage, each supports 4x 10/25G or 1x 40/100G Ethernet, other high speed serial standards possible (JESD-204B/C, Infiniband, etc)
 - 2 SFF-TA-1002 1C connectors with a GTY quad including both reference clocks, ? 3.3 V HD IO, ? 1.8 V HP IO, 3.3 V, 1.8 V, 12 V power
 - 72 bit DDR4-2666 (-2 speed grade) or DDR4-2400 (-1 speed grade) interface, 9 GiB, PCB supports 18 GiB
 - VCTCXO refefence clock, able to lock to many sources including recovered clocks from GTYs, 10 MHz
 - MAX2871 clock generators, defaults are 500 MHz to global clock, 257.8125 MHz to GTY quads
 - Cabline VS-II 40 pin expansion connector with ? 1.8 V IO (AU25P-KU5P only)
 - SPI configuration flash - 256 Mib
 - 12 V power input on 2.1x5.5 mm barrel jack
 - designed with Kicad
 - status: in progress

## Expansion connectors (small, medium)

These have proven to be somewhat unreliable with the short stiff flex interfaces.

These connectors are TE Connectivity 4-2328724-5 which is an 0.3 mm
pitch FPC connector. The back row (all odd numbered pins except 1, 45)
are all ground.  The pinout gets mirrored by the flex circuit
connecting two board.

All IO banks are 1.8 V powered.

IO7 is connected to a clock capable input pin.

Some IO connectors may support LVDS, other differential standards.

The host is the FPGA board. It provides power at 1.8 V (250 mA) and
3.6 V (500 mA).

Jumpers are in the hw-flex folder. They can be fabricated on OSHPark's
flex process and should be approximately 50 ohms impedance.

|Pin host|Pin device|Function|
|--------|----------|--------|
|   1,2  |   44,45  | 3.6 V  |
|   4    |     42   | IO 0   |
|   6    |     42   | IO 1   |
|   8    |     42   | IO 2   |
|   10   |     42   | IO 3   |
|   12   |     42   | IO 4   |
|   14   |     42   | IO 5   |
|   16   |     42   | IO 6   |
|   18   |     42   | IO 7 CLK |
|   20   |     42   | IO 8   |
|   22   |     42   | IO 9   |
|   24   |     42   | IO 10  |
|   26   |     42   | IO 11  |
|   28   |     42   | IO 12  |
|   30   |     42   | IO 13  |
|   32   |     42   | IO 14  |
|   34   |     42   | IO 15  |
|   36   |     42   | IO 16  |
|   38   |     42   | IO 17  |
|   40   |     42   | IO 18  |
|   42   |     42   | IO 19  |
|  44,45 |    1,2   | 1.8 V  |

## Expansion boards:
 - [AD9628 dual 12 bit 105 MSPS ADC](https://gitlab.com/harmoninstruments/ad9628-module)
 - [LTC2208 16 bit 130 MSPS ADC](https://gitlab.com/harmoninstruments/ltc2208-module)

## HDL and firmware
This is a work in progress. [Amaranth HDL](https://github.com/amaranth-lang/) is being used for the HDL. A Risc-V soft processor provides management functionality.

## License

The hardware is licensed under the [CERN OHL 2 Permissive](cern_ohl_p_v2.txt).

The HDL and software is licensed under [BSD-2-Clause-NetBSD](BSD2.txt) to match the [Amaranth HDL](https://github.com/amaranth-lang/) license.