#include <cstdint>
#include <unistd.h>
#include <array>

static std::array<uint8_t, 6> our_mac;

static constexpr uint16_t swap16(uint16_t x) { return __builtin_bswap16(x); }

class IPv4Address {
public:
        uint32_t a;
        constexpr IPv4Address(uint8_t w, uint8_t x, uint8_t y, uint8_t z)
            : a(w | (x << 8) | (y << 16) | (z << 24)) {}
        constexpr uint32_t val32() { return a; }
        constexpr uint32_t checksum() {
                return swap16(a & 0xFFFF) + swap16(a >> 16);
        }
};

static_assert(sizeof(IPv4Address) == 4);

static IPv4Address our_ip{10, 42, 0, 2};

enum class EtherType : uint16_t {
        ARP = swap16(0x0806),
        IPv4 = swap16(0x0800),
        IPv6 = swap16(0x86DD),
        FLOW = swap16(0x8808),
        PTP = swap16(0x88F7),
};

static inline uint32_t in32(const uint32_t p) {
        __sync_synchronize();
        return *reinterpret_cast<volatile uint32_t *>(p);
}

static inline void out32(const uint32_t p, uint32_t val) {
        __sync_synchronize();
        *reinterpret_cast<volatile uint32_t *>(p) = val;
}

// 57 bit unique ID
static void get_dna(std::array<uint8_t, 8> &d) {
        uint32_t r = 0x1000000C;
        out32(r, 0);
        for (int i = 0; i < 8; i++) {
                uint8_t byte = 0;
                for (int j = 0; j < 8; j++) {
                        int bit = in32(r);
                        byte = (byte << 1) | bit;
                }
                d[i] = byte;
        }
}

static inline uint32_t get_counter() { return in32(0x10000000); }
static inline uint32_t get_rx_fifo() { return in32(0x30002000); }

static void wait_tx() {
        for (;;) {
                if (in32(0x20002000) == 0)
                        return;
        }
}

static inline void set_rx_optr(uint32_t x) { out32(0x30000000, x); }
static inline void led(uint32_t x) { out32(0x10000004, x); }

static void copy_mac(void *d, const std::array<uint8_t, 6> &s) {
        for (int i = 0; i < 3; i++) {
                reinterpret_cast<uint16_t *>(d)[i] =
                    reinterpret_cast<const uint16_t *>(&s[0])[i];
        }
}

static void copy_mac(void *d, void *s) {
        for (int i = 0; i < 3; i++) {
                reinterpret_cast<uint16_t *>(d)[i] =
                    reinterpret_cast<const uint16_t *>(s)[i];
        }
}

static void copy_ip(void *d, const IPv4Address &s) {
        const uint16_t *s16 = reinterpret_cast<const uint16_t *>(&s.a);
        for (int i = 0; i < 2; i++) {
                reinterpret_cast<uint16_t *>(d)[i] = s16[i];
        }
}

static void copy_ip(void *d, const void *s) {
        for (int i = 0; i < 2; i++) {
                reinterpret_cast<uint16_t *>(d)[i] =
                    reinterpret_cast<const uint16_t *>(s)[i];
        }
}

static void memset16(void *s, int c, size_t n) {
        for (size_t i = 0; i < n; i++)
                reinterpret_cast<uint16_t *>(s)[i] = c;
}

static void memcpy32(void *d, const void *s, size_t n) {
        n = (n + 3) >> 2;
        for (size_t i = 0; i < n; i++)
                reinterpret_cast<uint32_t *>(d)[i] =
                    reinterpret_cast<const uint32_t *>(s)[i];
}

static void send_packet(uint32_t size) {
        if (size < 60)
                size = 60;
        out32(0x20001000, size + 2);
}

int constexpr strlen(const char *str) { return *str ? 1 + strlen(str + 1) : 0; }

static uint16_t finalize_checksum(uint32_t csum) {
        for (int i = 0; i < 2; i++)
                if (csum > 65535)
                        csum = (csum & 0xFFFF) + (csum >> 16);
        return 0xFFFF ^ swap16(csum);
}

struct EthernetPacket {
        uint16_t filler1;
        uint8_t dest[6];
        uint8_t src[6];
        EtherType ethertype;
};
static_assert(sizeof(EthernetPacket) == 16);

enum class ARP_HTYPE : uint16_t { Ethernet = swap16(1) };
enum class ARP_PTYPE : uint16_t { IPv4 = swap16(0x800) };
enum class ARP_HTYPE_PTYPE : uint32_t {
        EthernetIPv4 = swap16(1) | (swap16(0x800) << 16)
};

class __attribute__((__packed__)) ARPPacket {
public:
        EthernetPacket ethernet;
        ARP_HTYPE_PTYPE hptype;
        uint8_t hlen;
        uint8_t plen;
        uint16_t opcode;
        uint8_t source_mac[6];
        uint8_t source_ip[4]; // unaligned as 32
        uint8_t target_mac[6];
        uint8_t target_ip[4];
        uint32_t filler[5];
};

static_assert(sizeof(ARPPacket) == 64);

void send_arp() {
        wait_tx();
        auto &p = *reinterpret_cast<ARPPacket *>(0x20000000);
        memset16(p.ethernet.dest, 0xFFFF, 3); // broadcast
        copy_mac(p.ethernet.src, our_mac);
        p.ethernet.ethertype = EtherType::ARP;
        p.hptype = ARP_HTYPE_PTYPE::EthernetIPv4;
        p.hlen = 6;
        p.plen = 4;
        p.opcode = swap16(1);
        copy_mac(p.source_mac, our_mac);
        memset16(p.source_ip, 0, 2);
        memset16(p.target_mac, 0x0, 3); // probe
        copy_ip(p.target_ip, our_ip);
        send_packet(60);
}

enum class IP_Protocol : uint8_t { ICMP = 1, TCP = 6, UDP = 17 };

class IPv4Packet {
public:
        EthernetPacket ethernet;
        uint16_t header_len;
        uint16_t total_len;
        uint32_t id_flags_frag_off;
        uint8_t ttl;
        IP_Protocol protocol;
        uint16_t checksum;
        IPv4Address source_ip;
        IPv4Address dest_ip;
        void write_header(IPv4Address dest_ip, IPv4Address source_ip,
                          uint16_t a_len, IP_Protocol a_protocol);
};

static_assert(sizeof(IPv4Packet) == 36);

void IPv4Packet::write_header(IPv4Address a_dest_ip, IPv4Address a_source_ip,
                              uint16_t a_len, IP_Protocol a_protocol) {
        header_len = 0x45;               // 0x4500
        uint32_t csum = 0x4500 + 0xFF00; // header_len, ttl
        csum += static_cast<int>(a_protocol);
        uint16_t tmp_total_len = a_len + 20;
        csum += tmp_total_len;
        total_len = swap16(tmp_total_len);
        id_flags_frag_off = 0;
        ttl = 255;
        protocol = a_protocol;
        source_ip = a_source_ip;
        dest_ip = a_dest_ip;
        csum += a_source_ip.checksum();
        csum += a_dest_ip.checksum();
        checksum = finalize_checksum(csum);
}

class UDPv4Packet {
public:
        IPv4Packet ip;
        uint16_t source_port;
        uint16_t dest_port;
        uint16_t len;      // data + this 8 byte header
        uint16_t checksum; // 0 to ignore
        uint32_t data[1];  // data goes here
        void write_header(uint16_t a_dest_port, uint16_t a_source_port,
                          size_t a_len) {
                source_port = a_source_port;
                dest_port = a_dest_port;
                len = swap16(a_len + 8);
                checksum = 0;
        }
};

static_assert(sizeof(UDPv4Packet) == 48);

class ICMPv4Packet {
public:
        IPv4Packet ip;
        uint8_t type;
        uint8_t code;
        uint16_t checksum;
        uint32_t rest_of_header;
        uint8_t data[1];
};

static void send_udp(IPv4Address dest_ip, uint16_t dest_port,
                     uint16_t source_port, size_t len) {
        wait_tx();
        auto &p = *reinterpret_cast<UDPv4Packet *>(0x20000000);
        p.ip.ethernet.ethertype = EtherType::IPv4;
        p.ip.write_header(dest_ip, our_ip, len + 8, IP_Protocol::UDP);
        p.write_header(dest_port, source_port, len);
        send_packet(42 + len);
}

class Timer {
private:
        uint32_t begin;

public:
        Timer() { reset(); }
        void reset();
        uint32_t elapsed();
};

void Timer::reset() { begin = get_counter(); }
// return the elapsed time since reset in 8 ns units
uint32_t Timer::elapsed() { return get_counter() - begin; }
/*
void delay_clocks(const uint32_t clocks) {
        Timer t;
        while (t.elapsed() < clocks)
                __sync_synchronize();
                }*/

int ledstate = 1;

bool handle_arp(ARPPacket &p) {
        if (p.hptype != ARP_HTYPE_PTYPE::EthernetIPv4)
                return 0;
        if (p.hlen != 6)
                return 0;
        if (p.plen != 4)
                return 0;
        if (p.opcode != swap16(1)) // not a request
                return 0;
        auto &t = *reinterpret_cast<ARPPacket *>(0x20000000);
        // check if it's for us
        wait_tx();
        copy_mac(t.ethernet.dest, p.ethernet.src);
        copy_mac(t.ethernet.src, our_mac);
        t.ethernet.ethertype = EtherType::ARP;
        t.hptype = ARP_HTYPE_PTYPE::EthernetIPv4;
        t.hlen = 6;
        t.plen = 4;
        t.opcode = __builtin_bswap16(2);
        copy_mac(t.source_mac, our_mac);
        copy_ip(t.source_ip, our_ip);
        copy_mac(t.target_mac, p.source_mac);
        copy_ip(t.target_ip, p.source_ip);
        for (int i = 0; i < 5; i++)
                t.filler[i] = 0;
        send_packet(60);
        return 1;
}

static void handle_locate(UDPv4Packet &p) {
        if (p.data[0] != 0xDEADBEEF)
                return;
        auto &ptx = *reinterpret_cast<UDPv4Packet *>(0x20000000);
        ptx.data[0] = 0xCAFECAFE;
        for (int i = 0; i < 15; i++)
                ptx.data[1 + i] = p.data[i];
        copy_mac(ptx.ip.ethernet.dest, p.ip.ethernet.src);
        copy_mac(ptx.ip.ethernet.src, our_mac);
        send_udp(p.ip.source_ip, p.source_port, p.source_port, 64);
}

static void handle_repeat(UDPv4Packet &p) {
        wait_tx();
        int bytes = swap16(p.len) - 8;
        memcpy32((void *)0x20000000, p.data, bytes);
        send_packet(bytes - 2);
}

static void handle_memory_access(UDPv4Packet &p) {
        uint32_t *mem = reinterpret_cast<uint32_t *>(p.data[0] & ~3);
        auto &ptx = *reinterpret_cast<UDPv4Packet *>(0x20000000);
        ptx.data[0] = p.data[0];
        if (p.data[0] & 1) {
                *mem = p.data[1];
        }
        if (p.data[0] & 2) {
                ptx.data[1] = *mem;
        }
        copy_mac(ptx.ip.ethernet.dest, p.ip.ethernet.src);
        copy_mac(ptx.ip.ethernet.src, our_mac);
        send_udp(p.ip.source_ip, p.source_port, p.source_port, 8);
}

void handle_flash_access(UDPv4Packet &p) {
        size_t count = swap16(p.len) - 8;
        auto &ptx = *reinterpret_cast<UDPv4Packet *>(0x20000000);
        uint8_t *rxd = reinterpret_cast<uint8_t *>(p.data);
        uint8_t *txd = reinterpret_cast<uint8_t *>(ptx.data);
        // volatile uint32_t &r =
        //    *reinterpret_cast<volatile uint32_t *>(0x10000010);
        const uint32_t r = 0x10000010;
        for (size_t i = 0; i < count; i++) {
                out32(r, rxd[i]);
                for (;;) {
                        uint32_t rv = in32(r);
                        if (rv < 0x100) {
                                txd[i] = rv;
                                break;
                        }
                }
        }
        out32(r, 0x100); // set CS
        copy_mac(ptx.ip.ethernet.dest, p.ip.ethernet.src);
        copy_mac(ptx.ip.ethernet.src, our_mac);
        send_udp(p.ip.source_ip, p.source_port, p.source_port, count);
}

bool handle_udp(UDPv4Packet &p) {
        if (p.dest_port == swap16(9)) {
                handle_locate(p);
                return 1;
        }
        if (p.dest_port == swap16(42001)) {
                handle_repeat(p);
                return 1;
        }
        if (p.dest_port == swap16(42002)) {
                handle_memory_access(p);
                return 1;
        }
        if (p.dest_port == swap16(42003)) {
                handle_flash_access(p);
                return 1;
        }
        return 0;
}

bool handle_icmp_echo(ICMPv4Packet &p, uint32_t size) {
        auto &ptx = *reinterpret_cast<ICMPv4Packet *>(0x20000000);
        if (ptx.code != 0)
                return 0;
        wait_tx();
        // Ethernet
        copy_mac(ptx.ip.ethernet.dest, p.ip.ethernet.src);
        copy_mac(ptx.ip.ethernet.src, our_mac);
        ptx.ip.ethernet.ethertype = EtherType::IPv4;
        // IPv4
        ptx.ip.write_header(p.ip.source_ip, our_ip, size - 34,
                            IP_Protocol::ICMP);
        // ICMP
        ptx.type = 0;
        ptx.code = 0;
        memcpy32(&ptx.rest_of_header, &p.rest_of_header, size - 38);
        uint16_t *d16 = reinterpret_cast<uint16_t *>(&p.rest_of_header);
        uint32_t checksum = 0;
        size_t count = (size - 38) >> 1;
        while (count--)
                checksum += swap16(*d16++);
        ptx.checksum = finalize_checksum(checksum);
        send_packet(size);
        return 1;
}

bool handle_icmp(ICMPv4Packet &p, uint32_t size) {
        if (p.type == 8)
                return handle_icmp_echo(p, size);
        return 0;
}

bool handle_ip(IPv4Packet &p, uint32_t size) {
        if (p.protocol == IP_Protocol::UDP) {
                UDPv4Packet *pkt = reinterpret_cast<UDPv4Packet *>(&p);
                return handle_udp(*pkt);
        }
        if (p.dest_ip.val32() != our_ip.val32())
                return 0;
        if (p.protocol == IP_Protocol::ICMP) {
                ICMPv4Packet *pkt = reinterpret_cast<ICMPv4Packet *>(&p);
                return handle_icmp(*pkt, size);
        }
        return 0;
}

void handle_packet() {
        uint32_t pktinfo = get_rx_fifo();
        if (pktinfo == 0)
                return;
        ledstate ^= 1;
        led(ledstate);
        bool handled = 0;
        uint32_t offset = pktinfo & 0xFFF;
        uint32_t size = (pktinfo >> 12);
        EthernetPacket &pkt =
            *reinterpret_cast<EthernetPacket *>(0x30000000 + offset);
        if (pkt.ethertype == EtherType::ARP) {
                handled = handle_arp(reinterpret_cast<ARPPacket &>(pkt));
        } else if (pkt.ethertype == EtherType::IPv4) {
                handled = handle_ip(reinterpret_cast<IPv4Packet &>(pkt), size);
        } else {
        }
        if (!handled) {
                auto &ptx = *reinterpret_cast<UDPv4Packet *>(0x20000000);
                memcpy32(&ptx.data, &pkt, size + 2);
                memset16(ptx.ip.ethernet.dest, 0xFFFF, 3); // broadcast
                copy_mac(ptx.ip.ethernet.src, our_mac);
                send_udp(IPv4Address(255, 255, 255, 255), swap16(42000),
                         swap16(1200), size + 2);
        }
        set_rx_optr(offset + size);
}

extern "C" void main(void) {
        std::array<uint8_t, 8> dna;
        get_dna(dna);
        our_mac[0] = 0x2 | (dna[0] << 2);
        our_mac[1] = dna[0] | dna[1];
        our_mac[2] = dna[1] | dna[2];
        our_mac[3] = dna[4] | dna[3];
        our_mac[4] = dna[5];
        our_mac[5] = dna[6] | dna[7];

        Timer t;
        while (1) {
                if (t.elapsed() > 125000000) {
                        send_arp();
                        t.reset();
                }
                handle_packet();
        }
}
