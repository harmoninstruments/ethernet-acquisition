EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Rear USB Flex"
Date "2019-02-21"
Rev "1"
Comp "Harmon Instruments, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4850 4300 4800 4300
Connection ~ 4800 4300
Wire Wire Line
	4800 4300 4800 4400
Wire Wire Line
	4850 4200 4800 4200
Connection ~ 4800 4200
Wire Wire Line
	4800 4200 4800 4300
Wire Wire Line
	4850 4100 4800 4100
Connection ~ 4800 4100
Wire Wire Line
	4800 4100 4800 4200
Wire Wire Line
	4850 4000 4800 4000
Connection ~ 4800 4000
Wire Wire Line
	4800 4000 4800 4100
Wire Wire Line
	4850 3900 4800 3900
Connection ~ 4800 3900
Wire Wire Line
	4800 3900 4800 4000
Wire Wire Line
	4850 3800 4800 3800
Wire Wire Line
	4800 3800 4800 3900
$Comp
L combined:ground #PWR01
U 1 1 5C6FA5F1
P 4800 4950
F 0 "#PWR01" H 4800 4950 50  0001 C CNN
F 1 "ground" H 4800 4880 50  0001 C CNN
F 2 "" H 4800 4950 50  0001 C CNN
F 3 "" H 4800 4950 50  0000 C CNN
	1    4800 4950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4850 4400 4800 4400
Wire Wire Line
	4800 4400 4800 4500
Connection ~ 4800 4400
Wire Wire Line
	4850 3700 4800 3700
Connection ~ 4800 3700
Wire Wire Line
	4800 3700 4800 3800
Wire Wire Line
	4850 3600 4800 3600
Connection ~ 4800 3600
Wire Wire Line
	4800 3600 4800 3700
Wire Wire Line
	4850 3500 4800 3500
Connection ~ 4800 3500
Wire Wire Line
	4800 3500 4800 3600
Wire Wire Line
	4850 3400 4800 3400
Connection ~ 4800 3400
Wire Wire Line
	4800 3400 4800 3500
Wire Wire Line
	4850 3300 4800 3300
Connection ~ 4800 3300
Wire Wire Line
	4800 3300 4800 3400
Wire Wire Line
	4850 3200 4800 3200
Wire Wire Line
	4800 3200 4800 3300
Connection ~ 4800 3800
Wire Wire Line
	4850 3100 4800 3100
Connection ~ 4800 3100
Wire Wire Line
	4800 3100 4800 3200
Wire Wire Line
	4850 3000 4800 3000
Connection ~ 4800 3000
Wire Wire Line
	4800 3000 4800 3100
Wire Wire Line
	4850 2900 4800 2900
Wire Wire Line
	4800 2900 4800 3000
Connection ~ 4800 3200
Wire Wire Line
	6800 4300 6850 4300
Connection ~ 6850 4300
Wire Wire Line
	6850 4300 6850 4400
Wire Wire Line
	6800 4200 6850 4200
Connection ~ 6850 4200
Wire Wire Line
	6850 4200 6850 4300
Wire Wire Line
	6800 4100 6850 4100
Connection ~ 6850 4100
Wire Wire Line
	6850 4100 6850 4200
Wire Wire Line
	6800 4000 6850 4000
Connection ~ 6850 4000
Wire Wire Line
	6850 4000 6850 4100
Wire Wire Line
	6800 3900 6850 3900
Connection ~ 6850 3900
Wire Wire Line
	6850 3900 6850 4000
Wire Wire Line
	6800 3800 6850 3800
Wire Wire Line
	6850 3800 6850 3900
$Comp
L combined:ground #PWR02
U 1 1 5C70AC39
P 6850 4950
F 0 "#PWR02" H 6850 4950 50  0001 C CNN
F 1 "ground" H 6850 4880 50  0001 C CNN
F 2 "" H 6850 4950 50  0001 C CNN
F 3 "" H 6850 4950 50  0000 C CNN
	1    6850 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4400 6850 4400
Wire Wire Line
	6850 4400 6850 4500
Connection ~ 6850 4400
Wire Wire Line
	6800 3700 6850 3700
Connection ~ 6850 3700
Wire Wire Line
	6850 3700 6850 3800
Wire Wire Line
	6800 3600 6850 3600
Connection ~ 6850 3600
Wire Wire Line
	6850 3600 6850 3700
Wire Wire Line
	6800 3500 6850 3500
Connection ~ 6850 3500
Wire Wire Line
	6850 3500 6850 3600
Wire Wire Line
	6800 3400 6850 3400
Connection ~ 6850 3400
Wire Wire Line
	6850 3400 6850 3500
Wire Wire Line
	6800 3300 6850 3300
Connection ~ 6850 3300
Wire Wire Line
	6850 3300 6850 3400
Wire Wire Line
	6800 3200 6850 3200
Wire Wire Line
	6850 3200 6850 3300
Connection ~ 6850 3800
Wire Wire Line
	6800 3100 6850 3100
Connection ~ 6850 3100
Wire Wire Line
	6850 3100 6850 3200
Wire Wire Line
	6800 3000 6850 3000
Connection ~ 6850 3000
Wire Wire Line
	6850 3000 6850 3100
Wire Wire Line
	6800 2900 6850 2900
Wire Wire Line
	6850 2900 6850 3000
Connection ~ 6850 3200
Wire Wire Line
	5450 3350 6200 3350
Wire Wire Line
	6200 3450 5450 3450
Wire Wire Line
	5450 3550 6200 3550
Wire Wire Line
	6200 3650 5450 3650
Wire Wire Line
	5450 3750 6200 3750
Wire Wire Line
	6200 3850 5450 3850
Wire Wire Line
	5450 4050 6200 4050
Text Label 5700 3550 0    50   ~ 0
D6
Text Label 5700 3650 0    50   ~ 0
D7
Text Label 5700 3350 0    50   ~ 0
D4
Text Label 5700 3450 0    50   ~ 0
D5
Text Label 5700 3750 0    50   ~ 0
D8
Text Label 5700 3850 0    50   ~ 0
D9
Text Label 5700 4050 0    50   ~ 0
D11
Wire Wire Line
	5450 3950 6200 3950
Text Label 5700 3950 0    50   ~ 0
D10
Wire Wire Line
	5450 4250 6200 4250
Text Label 5700 4250 0    50   ~ 0
D13
Wire Wire Line
	5450 4150 6200 4150
Text Label 5700 4150 0    50   ~ 0
D12
Wire Wire Line
	5450 2850 6200 2850
Wire Wire Line
	6200 2950 5450 2950
Wire Wire Line
	5450 3050 6200 3050
Text Label 5700 3050 0    50   ~ 0
D1
Text Label 5700 2850 0    50   ~ 0
P0
Text Label 5700 2950 0    50   ~ 0
D0
Wire Wire Line
	5450 3150 6200 3150
Wire Wire Line
	6200 3250 5450 3250
Wire Wire Line
	5450 4550 6200 4550
Text Label 5700 4550 0    50   ~ 0
D16
Text Label 5700 3150 0    50   ~ 0
D2
Text Label 5700 3250 0    50   ~ 0
D3
Text Notes 5350 2350 0    50   ~ 0
D[n] are 50 ohm
$Comp
L connector:FPC_45_tail J1
U 1 1 5CB24574
P 5000 2700
F 0 "J1" H 5150 2750 50  0000 C CNN
F 1 "FPC_45_tail" H 5000 2750 50  0001 L CNN
F 2 "kicad_pcb:fpc_0.3_45_tail" H 5000 2750 50  0001 L CNN
F 3 "$PARTS/HF9MW/ENG_CD_2328724_3.pdf" V 5250 2700 50  0001 C CNN
	1    5000 2700
	1    0    0    -1  
$EndComp
$Comp
L connector:FPC_45_tail J2
U 1 1 5CB26577
P 6650 5100
F 0 "J2" H 6800 2650 50  0000 C CNN
F 1 "FPC_45_tail" H 6650 5150 50  0001 L CNN
F 2 "kicad_pcb:fpc_0.3_45_tail" H 6650 5150 50  0001 L CNN
F 3 "$PARTS/HF9MW/ENG_CD_2328724_3.pdf" V 6900 5100 50  0001 C CNN
	1    6650 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 4650 6200 4650
Text Label 5700 4650 0    50   ~ 0
D17
Wire Wire Line
	5450 4750 6200 4750
Text Label 5700 4750 0    50   ~ 0
D18
Wire Wire Line
	5450 4850 6200 4850
Text Label 5700 4850 0    50   ~ 0
D19
Wire Wire Line
	5450 4950 6200 4950
Text Label 5700 4950 0    50   ~ 0
P1
Wire Wire Line
	5450 4450 6200 4450
Text Label 5700 4450 0    50   ~ 0
D15
Wire Wire Line
	5450 4350 6200 4350
Text Label 5700 4350 0    50   ~ 0
D14
Wire Wire Line
	6800 4500 6850 4500
Connection ~ 6850 4500
Wire Wire Line
	6850 4500 6850 4600
Wire Wire Line
	6800 4600 6850 4600
Connection ~ 6850 4600
Wire Wire Line
	6850 4600 6850 4700
Wire Wire Line
	6800 4700 6850 4700
Connection ~ 6850 4700
Wire Wire Line
	6850 4700 6850 4800
Wire Wire Line
	6800 4800 6850 4800
Connection ~ 6850 4800
Wire Wire Line
	6850 4800 6850 4900
Wire Wire Line
	6800 4900 6850 4900
Connection ~ 6850 4900
Wire Wire Line
	4850 4900 4800 4900
Connection ~ 4800 4900
Wire Wire Line
	4850 4800 4800 4800
Connection ~ 4800 4800
Wire Wire Line
	4800 4800 4800 4900
Wire Wire Line
	4850 4700 4800 4700
Connection ~ 4800 4700
Wire Wire Line
	4800 4700 4800 4800
Wire Wire Line
	4850 4600 4800 4600
Connection ~ 4800 4600
Wire Wire Line
	4800 4600 4800 4700
Wire Wire Line
	4850 4500 4800 4500
Connection ~ 4800 4500
Wire Wire Line
	4800 4500 4800 4600
Wire Wire Line
	4800 4900 4800 4950
Wire Wire Line
	6850 4900 6850 4950
Wire Wire Line
	6800 2800 6800 2550
Wire Wire Line
	6800 2550 6200 2550
Wire Wire Line
	6200 2550 6200 2850
Connection ~ 6200 2850
Wire Wire Line
	5450 2850 5450 2550
Wire Wire Line
	5450 2550 4850 2550
Wire Wire Line
	4850 2550 4850 2800
Connection ~ 5450 2850
Wire Wire Line
	4850 5000 4850 5150
Wire Wire Line
	4850 5150 5450 5150
Wire Wire Line
	5450 5150 5450 4950
Connection ~ 5450 4950
Wire Wire Line
	6200 4950 6200 5150
Wire Wire Line
	6200 5150 6800 5150
Wire Wire Line
	6800 5150 6800 5000
Connection ~ 6200 4950
$EndSCHEMATC
