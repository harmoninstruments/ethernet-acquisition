#!/usr/bin/env python3

from __future__ import print_function
import sys
import math
from pcbnew import *

eri = 4.25 # relative permittivity
ero = 2.6 # relative permittivity
mm_per_m = 1000.0
s_per_ps = 1e-12
c = 2.998e8
scale_inner = 1.0/(c*mm_per_m*s_per_ps/math.sqrt(eri)) # ps per mm
scale_outer = 1.0/(c*mm_per_m*s_per_ps/math.sqrt(ero)) # ps per mm
print("scale =", scale_inner, scale_outer,  "ps per mm")

pcb = LoadBoard('ffvb676.kicad_pcb')
tracks = pcb.GetTracks()

td = {}
td_outer = {}
mod = pcb.FindFootprintByReference('U9')
pads = mod.Pads()
for pad in pads:
    name = pad.GetNetname()
    length = ToMM(pad.GetPadToDieLength())
    td[name] = length * scale_inner

for track in tracks:
    name = track.GetNetname()
    length = ToMM(track.GetLength())
    if(track.GetLayerName() not in ['F.Cu','B.Cu']):
        length *= scale_inner
        if name in td:
            td[name] += length
        else:
            td[name] = length
    else:
        length *= scale_outer
        if name in td_outer:
            td_outer[name] += length
        else:
            td_outer[name] = length

def plist(nlist, include_outer=True):
    minl = 10000
    maxl = 0
    ref = td[nlist[0]]
    if include_outer:
        ref += td_outer[nlist[0]]
    print(f"ref = {nlist[0]}: {round(ref,1)} ps")
    for net in nlist[1:]:
        delay = td[net] - ref
        if include_outer:
            delay += td_outer[net]
        delay = round(delay, 1)
        print(net, delay)
        maxl = max(maxl, delay)
        minl = min(minl, delay)
    print(f"max = {maxl} min = {minl}")
"""
for d in ['/DAC0/', '/DAC1/']:
    nets = []
    for net in td:
        if d in net:
            if net[6] != 'D' or 'DAC' in net[6:]:
                continue
            nets += [net]
    print("\nmain DAC, spec is 0 +- 50 ps:")
    plist([d+"DCLK+"] + nets)
"""
nets = []
for s in "CK- ~{CS} ~{ACT} ODT CKE BG0 BG1 BA0 BA1".split(' '):
    nets += ["/ddr/"+s]
for i in range(17):
    nets += [f"/ddr/A{i}"]

print("DDR4 control")
plist(["/ddr/CK+"] + nets, include_outer=False)

for i in range(9):
    nets=[f"/ddr/DQS{i}+"]
    nets+=[f"/ddr/DQS{i}-"]
    nets+=[f"/ddr/DM{i}"]
    for j in range(8):
        nets += [f"/ddr/DQ{i*8+j}"]
    print(f"DDR4 byte {i}")
    plist(nets, include_outer=True)
