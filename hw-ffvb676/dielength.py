#!/usr/bin/env python3

# kicad file is first arg, csv of lengths is 2nd
import sys, math
from pcbnew import *

pcb = LoadBoard(sys.argv[1])

lengths = {}

with open(sys.argv[2]) as f:
    for l in f:
        d = l.split(',')
        try:
            lengths[d[0]] = (float(d[1]) + float(d[2])) * 0.5
        except:
            pass

mod = pcb.FindFootprintByReference('U9')
pads = mod.Pads()

for pad in pads:
    name = pad.GetPadName()
    if name in lengths:
        # convert from picoseconds to mm, all on inner
        Er = 4.25
        c_m_per_s = 3e8
        c_mm_per_ps = c_m_per_s * 1e-12 * 1e3
        len_mm = lengths[name] * c_mm_per_ps / sqrt(Er)
        pad.SetPadToDieLength(FromMM(len_mm))

pcb.Save("./mod.kicad_pcb")
