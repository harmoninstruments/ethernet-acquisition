#!/usr/bin/env python3

from pcbnew import *

pcb = LoadBoard('ffvb676.kicad_pcb')
mod = pcb.FindFootprintByReference('U9')
pads = mod.Pads()

nets = {}

for pad in pads:
    name = pad.GetNetname()
    num = pad.GetNumber()
    print(name, num)
    nets[name] = num

print(nets)

def se(base, n, altname, dir='o'):
    pinlist = ""
    for i in range(n):
        ball = nets[base] if n == 1 else nets[f"{base}{i}"]
        pinlist += ball + ' '
    print(f"Subsignal(\"{altname}\", Pins(\"{pinlist[:-1]}\", dir='{dir}')),")

def dp(base, n, altname, dir='o'):
    pinlistp = ""
    pinlistn = ""
    for i in range(n):
        ball = nets[base+'+'] if n == 1 else nets[f"{base}{i}+"]
        pinlistp += ball + ' '
        ball = nets[base+'-'] if n == 1 else nets[f"{base}{i}-"]
        pinlistn += ball + ' '
    print(f"Subsignal(\"{altname}\", DiffPairs(\"{pinlistp[:-1]}\", \"{pinlistn[:-1]}\", dir='{dir}')),")

se("/ddr/DQ", 72, 'dq', dir='io')
se("/ddr/DM", 9, 'dm', dir='o')
se("/ddr/A", 17, 'a', dir='o')
se("/ddr/~{ACT}", 1, "act", dir='o')
se("/ddr/~{CS}", 1, "cs", dir='o')
se("/ddr/~{ALERT}", 1, "alert", dir='i')
se("/ddr/ODT", 1, "odt", dir='o')
se("/ddr/CKE", 1, "cke")
se("/ddr/DDRRST", 1, "reset")
se("/ddr/BG", 2, "bg")
se("/ddr/BA", 2, "ba")
dp("/ddr/DQS", 9, "dqs", dir='io')
dp("/ddr/CK", 1, "ck")

se("/LED", 4, "led")

for i in [225, 227]:
    print(f"{i} quad")
    se(f"/SCL{i}", 1, "scl")
    se(f"/SDA{i}", 1, "sda")
    se(f"/~{{SFPRST{i}}}", 1, "reset")

for i in range(16):
    print(f'Resource("mgt", {i},')
    print("    ", end='')
    dp(f"/MGT/TX{i}", 1, "tx", dir='o')
    print("    ", end='')
    dp(f"/MGT/RX{i}", 1, "rx", dir='i')
    print("),")
