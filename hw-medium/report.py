#!/usr/bin/env python3

from __future__ import print_function
import sys
import math
from pcbnew import *

eri = 4.0 # relative permittivity
ero = 2.6 # relative permittivity
mm_per_m = 1000.0
s_per_ps = 1e-12
c = 2.998e8
scale_inner = 1.0/(c*mm_per_m*s_per_ps/math.sqrt(eri)) # ps per mm
scale_outer = 1.0/(c*mm_per_m*s_per_ps/math.sqrt(ero)) # ps per mm
print("scale =", scale_inner, scale_outer,  "ps per mm")

pcb = LoadBoard('medium.kicad_pcb')
tracks = pcb.GetTracks()
td = {}
netpad = {}
mod = pcb.FindModuleByReference('U3')
pads = mod.Pads()
for pad in pads:
    name = pad.GetNetname()
    length = ToMM(pad.GetPadToDieLength())
    td[name] = length
    netpad[name] = pad.GetName()

for track in tracks:
    name = track.GetNetname()
    length = ToMM(track.GetLength())
    if(track.GetLayerName() not in ['F.Cu','B.Cu']):
        length *= scale_inner
    else:
        length *= scale_outer
    if name in td:
        td[name] += length
    else:
        td[name] = length

def plist(nlist):
    minl = 10000
    maxl = 0
    for net in nlist:
        delay = round(td[net],2)
        print(net, delay)
        maxl = max(maxl, delay)
        minl = min(minl, delay)
    print("max = ", maxl, "min = ", minl)

for d in ['RX', 'TX']:
    nets = []
    for net in td:
        if d in net:
            nets += [net]
    plist(nets)

def ioconn(refdes):
    print("IO", refdes)
    mod = pcb.FindModuleByReference(refdes)
    pads = mod.Pads()
    nets = []
    pins = []
    for pad in pads:
        name = pad.GetNetname()
        if 'IO' in name:
            nets += [name]
            pins.append((netpad[name]))
    print(" ".join(pins[::-1]))
    plist(nets)

ioconn("J1")
ioconn("J2")
ioconn("J3")
ioconn("J4")
ioconn("J5")
