# Copyright 2019-2023 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth.build import *
from amaranth.vendor.xilinx import *
import sys
sys.path.append("../fpga/harmon-instruments-open-hdl/")
import time
from amaranth import *

attrs_33 = Attrs(IOSTANDARD="LVCMOS33", SLEW="SLOW", DRIVE="4")
attrs_33_pu = Attrs(IOSTANDARD="LVCMOS33", SLEW="SLOW", DRIVE="4", PULLUP="TRUE")
attrs_18_slow = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4")
attrs_18_slow_pu = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4", PULLUP="TRUE")
attrs_18_fast = Attrs(IOSTANDARD="LVCMOS18", SLEW="FAST", DRIVE="4")
attrs_18_dci = Attrs(IOSTANDARD="HSTL_I_DCI_18", SLEW="FAST")

class FFVB676Platform(XilinxPlatform):
    device      = "xcau10p"#ku5p"
    package     = "ffvb676"
    speed       = "2-e"
    default_clk = "sync"
    resources = [
        Resource(
            "clk100ddr",
            0,
            DiffPairs("AB21", "AC21", dir="i"),
            Clock(500e6),
            Attrs(IOSTANDARD="DIFF_SSTL12_DCI", ODT="RTT_48", DQS_BIAS="TRUE")
        ),
        Resource(
            "clk500",
            0,
            DiffPairs("C18", "C19", dir="i"),
            Clock(500e6),
            Attrs(IOSTANDARD="LVDS", DQS_BIAS="TRUE", EQUALIZATION="EQ_LEVEL0",
                  DIFF_TERM_ADV="TERM_100"),
        ),
        Resource("clk0_225", 0, DiffPairs("P7", "P6", dir="i"), Clock(257.8125e6)),
        Resource("clk1_225", 0, DiffPairs("P7", "P6", dir="i"), Clock(500e6)),
        Resource("clk0_227", 0, DiffPairs("K7", "K6", dir="i"), Clock(257.8125e6)),
        Resource("led", 0, Pins("C13 A14 B14 C14", dir='o'), attrs_33),

        Resource("qsfp", 0, #225 quad
                 Subsignal("scl", Pins("B9", dir='o')),
                 Subsignal("sda", Pins("A9", dir='io')),
                 Subsignal("reset", Pins("A10", dir='o')),
                 attrs_33,
                 ),
        Resource("qsfp", 1, #227
                 Subsignal("scl", Pins("AB16", dir='o')),
                 Subsignal("sda", Pins("AA15", dir='io')),
                 Subsignal("reset", Pins("Y16", dir='o')),
                 attrs_33,
                 ),
        Resource("flash", 0,
                 Subsignal("cs", Pins("C23", dir="o")),
                 Subsignal("cipo", Pins("A25", dir="i")),
                 Subsignal("copi", Pins("B24", dir="o")),
                 attrs_18_slow_pu,
        ),
        Resource(
            "ddr", 0,
            Subsignal("dq", Pins("U25 P26 N24 R25 R26 P25 T25 P24 W19 U21 U20 T20 W20 T22 U22 T23 Y26 W24 AA25 AA24 Y25 V24 W23 V23 AD24 AD25 AB24 AB25 AF24 AB26 AF25 AC24 P21 P20 R21 R20 P19 N19 N22 N21 M21 M20 J20 J19 J21 K21 K20 L20 M26 M25 K26 K25 K23 K22 J24 J23 H24 H23 J26 J25 G26 H26 H22 H21 E23 F23 D25 D24 C26 D26 B26 B25", dir='io')),
            Subsignal("dm", Pins("T24 U19 Y22 AE25 R22 L18 L22 G24 E25", dir='o')),
            Subsignal("a", Pins("AA18 AB19 AC17 AB20 AA19 AE17 Y21 AB17 AE18 AC19 AA20 AA17 AE21 AD16 AD23 AF22 AF19", dir='o')),
            Subsignal("act", Pins("AD21", dir='o')),
            Subsignal("cs", Pins("AD19", dir='o')),
            Subsignal("alert", Pins("AE26", dir='i'), Attrs(IOSTANDARD="LVCMOS12")),
            Subsignal("odt", Pins("AE23", dir='o')),
            Subsignal("cke", Pins("AE22", dir='o')),
            Subsignal("reset", Pins("AF17", dir='o')),
            Subsignal("bg", Pins("AC23 AD18", dir='o')),
            Subsignal("ba", Pins("AC22 AF18", dir='o')),
            Subsignal("dqs", DiffPairs("U26 V21 W25 AC26 N23 M19 L24 F24 D23", "V26 V22 W26 AD26 P23 L19 L25 F25 C24", dir='io')),
            Subsignal("ck", DiffPairs("AE20", "AD20", dir='o')),
        ),
        Resource("mgt", 0,
                 Subsignal("tx", DiffPairs("AC4", "AC5", dir='o')),
                 Subsignal("rx", DiffPairs("AB1", "AB2", dir='i')),
                 ),
        Resource("mgt", 1,
                 Subsignal("tx", DiffPairs("AD6", "AD7", dir='o')),
                 Subsignal("rx", DiffPairs("AD2", "AD1", dir='i')),
                 ),
        Resource("mgt", 2,
                 Subsignal("tx", DiffPairs("AE8", "AE9", dir='o')),
                 Subsignal("rx", DiffPairs("AE3", "AE4", dir='i')),
                 ),
        Resource("mgt", 3,
                 Subsignal("tx", DiffPairs("AF6", "AF7", dir='o')),
                 Subsignal("rx", DiffPairs("AF1", "AF2", dir='i')),
                 ),
        Resource("mgt", 4,
                 Subsignal("tx", DiffPairs("AA5", "AA4", dir='o')),
                 Subsignal("rx", DiffPairs("Y2", "Y1", dir='i')),
                 ),
        Resource("mgt", 5,
                 Subsignal("tx", DiffPairs("W5", "W4", dir='o')),
                 Subsignal("rx", DiffPairs("V2", "V1", dir='i')),
                 ),
        Resource("mgt", 6,
                 Subsignal("tx", DiffPairs("U5", "U4", dir='o')),
                 Subsignal("rx", DiffPairs("T2", "T1", dir='i')),
                 ),
        Resource("mgt", 7,
                 Subsignal("tx", DiffPairs("R5", "R4", dir='o')),
                 Subsignal("rx", DiffPairs("P2", "P1", dir='i')),
                 ),
        Resource("mgt", 8,
                 Subsignal("tx", DiffPairs("N5", "N4", dir='o')),
                 Subsignal("rx", DiffPairs("M2", "M1", dir='i')),
                 ),
        Resource("mgt", 9,
                 Subsignal("tx", DiffPairs("L5", "L4", dir='o')),
                 Subsignal("rx", DiffPairs("K2", "K1", dir='i')),
                 ),
        Resource("mgt", 10,
                 Subsignal("tx", DiffPairs("J5", "J4", dir='o')),
                 Subsignal("rx", DiffPairs("H2", "H1", dir='i')),
                 ),
        Resource("mgt", 11,
                 Subsignal("tx", DiffPairs("G5", "G4", dir='o')),
                 Subsignal("rx", DiffPairs("F2", "F1", dir='i')),
                 ),
        Resource("mgt", 12,
                 Subsignal("tx", DiffPairs("F7", "F6", dir='o')),
                 Subsignal("rx", DiffPairs("D2", "D1", dir='i')),
                 ),
        Resource("mgt", 13,
                 Subsignal("tx", DiffPairs("E5", "E4", dir='o')),
                 Subsignal("rx", DiffPairs("C4", "C3", dir='i')),
                 ),
        Resource("mgt", 14,
                 Subsignal("tx", DiffPairs("D7", "D6", dir='o')),
                 Subsignal("rx", DiffPairs("B2", "B1", dir='i')),
                 ),
        Resource("mgt", 15,
                 Subsignal("tx", DiffPairs("B7", "B6", dir='o')),
                 Subsignal("rx", DiffPairs("A4", "A3", dir='i')),
                 ),
    ]
    mgt_invert_tx = [7]
    mgt_invert_rx = [0,3,6]
    connectors = [
        Connector(
            "gty224", 0,
            "U2 U1 V1 V2 W1 Y1 AA2 AA3 AB1 AC1 AD1 AE1 AE2 AE3 AF2 AF3 AF4 AF5 AE5 AE6"),
        Connector(
            "gty226", 1,
            "AC14 AD14 AE15 AD15 AB17 AC17 AD18 AC18 AF14 AF15 AE16 AD16 AD19 AC19 AF17 AE17 AF18 AE18 AF19 AF20"),
    ]

    constraints = ""

    def create_pblock(self, name, region):
        self.constraints += r"""
        create_pblock {pblock}
        resize_pblock [get_pblocks {pblock}] -add {{CLOCKREGION_{cr}:CLOCKREGION_{cr}}}\n
        """.format(pblock="pblock_"+name, cr=region)

    def add_to_pblock(self, name, cells):
        self.constraints += "add_cells_to_pblock [get_pblocks {pblock}] [get_cells -quiet [list {cells}]]\n".format(pblock="pblock_"+name, cells=cells)

    tcl_after_read = ""
    tcl_after_synth = r"""
            foreach cell [get_cells -quiet -hier -filter {hi.false_path != ""}] {
                set_false_path -from [get_property hi.false_path $cell] -to $cell
            }
            save_design
            """

    def toolchain_prepare(self, fragment, name, **kwargs):
        overrides = {
            "script_after_read": self.tcl_after_read,
            #"set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]",
            #"script_after_bitstream":
            #    "write_cfgmem -force -format bin -interface spix4 -size 16 "
            # "-loadbit \"up 0x0 {name}.bit\" -file {name}.bin".format(name=name),
            "add_constraints":
            #"set_property INTERNAL_VREF 0.750 [get_iobanks 14]\n"
            #"create_clock -name clk156_0__i -period 6.4 [get_nets clk156_0__p]\n"
            #"set_property INTERNAL_VREF 0.9 [get_iobanks 32]\n"
            #"set_property INTERNAL_VREF 0.9 [get_iobanks 15]\n"
            #"set_property CLOCK_DEDICATED_ROUTE BACKBONE ddr/ddr_core/inst/u_ddr4_infrastructure/gen_mmcme4.u_mmcme_adv_inst/CLKIN1\n"
            "set_property CONFIG_VOLTAGE 1.8 [current_design]\n"
            "set_property CONFIG_MODE {SPIx4} [current_design]\n"
            "set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]\n"
            "set_operating_conditions -ambient_temp 55.0\n" + self.constraints,
            "script_after_synth": self.tcl_after_synth,
        }
        return super().toolchain_prepare(fragment, name, **overrides, **kwargs)

"""
from clock import ClockGen
from XC7.DNA import DNA
from XC7.clockbuffers import *
from XC7.GTXE2_COMMON import *
from XC7.GTXE2_CHANNEL import *
from Ethernet.BlockSync64b66b import *
from Ethernet.Scrambler10GBASER import Descrambler
from Ethernet.XC7_40GBASER_TX import *
from Ethernet.XC7_10GBASER_TX import *
from Ethernet.TX_40GBASER import *
from Ethernet.TXbuf_32 import TXbuf_32
from Ethernet.RXbuf_32 import RXbuf_32
from Ethernet.RX_10GBASER import RX_10GBASER
from AXI.Stream import AXIStream
from Memory.FIFO import FIFO
from RiscV.RiscV import RiscV
"""
import struct

class DDR(Elaboratable):
    def __init__(self, reset):
        self.reset = reset # async reset
        self.complete = Signal()
        self.error = Signal()
        self.init_calib_complete = Signal()
    def elaborate(self, platform):
        pins = platform.request("ddr", dir={
            "dq":"-", "dm":"-", "a":"-", "act":"-", "cs":"-", "alert":"-", "odt":"-",
            "cke":"-", "reset":"-", "bg":"-", "ba":"-", "dqs":"-", "ck":"-"})
        clkpins = platform.request("clk100ddr", dir='-')
        m = Module()
        addr = Signal(30)
        cmd = Signal(3)
        en = Signal()
        wdf_data = Signal(576)
        wdf_mask = Signal(72)
        wdf_end = Signal()
        wdf_wren = Signal()
        rd_data = Signal(576)
        rd_data_end = Signal()
        rd_data_valid = Signal()
        app_ready = Signal()
        wdf_ready = Signal()
        reset = Signal()
        dbg_clk = Signal()
        wr_rd_complete = Signal()
        debug_bus = Signal(512)

        m.submodules.ddr_core = Instance(
            "ddr4_0",
            i_sys_rst=self.reset,
            i_c0_sys_clk_p=clkpins.p,
            i_c0_sys_clk_n=clkpins.n,
            o_c0_init_calib_complete=self.init_calib_complete,
            # chip IO
            o_c0_ddr4_act_n=pins.act,
            o_c0_ddr4_adr=pins.a,
            o_c0_ddr4_ba=pins.ba,
            o_c0_ddr4_bg=pins.bg,
            o_c0_ddr4_cke=pins.cke,
            o_c0_ddr4_odt=pins.odt,
            o_c0_ddr4_cs_n=pins.cs,
            o_c0_ddr4_ck_t=pins.ck.p,
            o_c0_ddr4_ck_c=pins.ck.n,
            o_c0_ddr4_reset_n=pins.reset,
            io_c0_ddr4_dm_dbi_n=pins.dm,
            io_c0_ddr4_dq=pins.dq,
            io_c0_ddr4_dqs_c=pins.dqs.n,
            io_c0_ddr4_dqs_t=pins.dqs.p,
            # User interface
            o_c0_ddr4_ui_clk=ClockSignal("ddr4_ui"),
            o_c0_ddr4_ui_clk_sync_rst=reset,
            i_c0_ddr4_app_addr=addr,
            i_c0_ddr4_app_cmd=cmd,
            i_c0_ddr4_app_en=en,
            i_c0_ddr4_app_hi_pri=C(0),
            i_c0_ddr4_app_wdf_data=wdf_data,
            i_c0_ddr4_app_wdf_end=wdf_end,
            i_c0_ddr4_app_wdf_wren=wdf_wren,
            o_c0_ddr4_app_rd_data=rd_data,
            o_c0_ddr4_app_rd_data_end=rd_data_end,
            o_c0_ddr4_app_rd_data_valid=rd_data_valid,
            o_c0_ddr4_app_rdy=app_ready,
            o_c0_ddr4_app_wdf_rdy=wdf_ready,
            o_dbg_bus=debug_bus,
            o_dbg_clk=dbg_clk,
        )

        m.submodules.testbench = Instance(
            "example_tb",
            p_APP_DATA_WIDTH=576,
            p_APP_ADDR_WIDTH=30,
            p_MEM_ADDR_ORDER="ROW_COLUMN_BANK",
            i_clk=ClockSignal("ddr4_ui"),
            i_rst=reset,
            i_app_rdy=app_ready,
            i_init_calib_complete=self.init_calib_complete,
            i_app_wdf_rdy=wdf_ready,
            i_app_rd_data_valid=rd_data_valid,
            i_app_rd_data=rd_data,
            o_app_en=en,
            o_app_cmd=cmd,
            o_app_addr=addr,
            o_app_wdf_mask=wdf_mask,
            o_app_wdf_data=wdf_data,
            o_app_wdf_end=wdf_end,
            o_app_wdf_wren=wdf_wren,
            o_compare_error=self.error,
            o_wr_rd_complete=self.complete,
        )
        return m


class FFVB676(Elaboratable):
    def elaborate(self, platform):
        gt_pins = []
        #for i in range(16):
        #    gt_pins += [platform.request("mgt", i, dir={"tx":'-', "rx":'-'})]
        #platform.create_pblock("X0Y0", "X0Y0")
        m = Module()
        m.domains.sync = ClockDomain(reset_less=True) # 500 MHz via BUFG
        m.domains.ddr4_ui = ClockDomain(reset_less=True) # 333 MHz from DDR4 core
        """
        m.domains.clk200 = ClockDomain(reset_less=True)
        m.domains.clk250 = ClockDomain(reset_less=True)
        m.domains.clk625 = ClockDomain(reset_less=True)
        m.domains.clk312 = ClockDomain(reset_less=True)
        m.domains.clk156 = ClockDomain(reset_less=True)
        m.domains.txclk = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFH
        m.domains.txclkg = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFHCE
        m.domains.rxclk0 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk0g = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFHCE
        m.domains.txclk4 = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFH
        m.domains.txclk4g = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFHCE
        m.domains.rxclk4 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk5 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk6 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk7 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        """
        clk500_ibuf = Signal()

        if 'ku5p' in platform.device:
            clkpins = platform.request("clk500", dir='-')
            clk500i = Signal()
            m.submodules.ibuf500 = Instance(
                "IBUFDS", p_IBUF_LOW_PWR="FALSE", i_I=clkpins.p, i_IB=clkpins.n, o_O=clk500i)
            m.submodules.bufg500 = Instance(
                "BUFG", i_I=clk500i, o_O=ClockSignal("sync"))
        else:
            m.d.comb += ClockSignal("sync").eq(ClockSignal("ddr4_ui"))

        reset = Signal(reset=1)
        count = Signal(32)
        m.d.sync += count.eq(count + 1)
        with m.If(count[-1]):
            m.d.sync += reset.eq(0)

        m.submodules.ddr = ddr = DDR(reset=reset)

        led = platform.request("led")
        m.d.comb += led.o.eq(Cat(ddr.complete, ddr.error, ddr.init_calib_complete, count[-1]))

        """
        m.submodules.ibuf_gt = Instance("IBUFDS_GTE2",
                                        i_I=clk156_pin.p, i_IB=clk156_pin.n,
                                        i_CEB=C(0),
                                        o_O=clk156_ibuf,
                                        o_ODIV2=Signal()
        )"""

        #for c in platform.iter_clock_constraints():
        #    print(c)

        #m.submodules.cpu = cpu

        platform.constraints += "create_clock -period 2.0 [get_nets {clk500i}]\n"

        return m

if __name__ == "__main__":
    p = FFVB676Platform()
    p.tcl_after_read += "import_files -norecurse ../build_cores/ddr4_0/ddr4_0.xci\n"
    p.tcl_after_read += "add_files -norecurse ../example_tb.sv\n"

    #p.add_resources(get_ltc2208(1))
    p.build(FFVB676(), do_program=False)
