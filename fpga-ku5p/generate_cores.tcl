create_project ku5p-cores build_cores -part xcku5p-ffvb676-2-e

create_ip -name ddr4 -vendor xilinx.com -library ip -version 2.2 -module_name ddr4_0 -dir build_cores
set_property -dict [list \
  CONFIG.ADDN_UI_CLKOUT1_FREQ_HZ {None} \
  CONFIG.C0.DDR4_DataMask {NO_DM_DBI_WR_RD} \
  CONFIG.C0.DDR4_DataWidth {72} \
  CONFIG.C0.DDR4_InputClockPeriod {10000} \
  CONFIG.C0.DDR4_MemoryPart {MT40A1G8SA-075} \
  CONFIG.System_Clock {Differential} \
                       ] [get_ips ddr4_0]

generate_target {instantiation_template} [get_ips]
update_compile_order -fileset sources_1
generate_target all [get_ips]
synth_ip [get_ips]

exit
