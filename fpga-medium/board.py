# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth.build import *
from amaranth.vendor.xilinx import *

attrs_lvds_i = Attrs(IOSTANDARD="LVDS", DIFF_TERM="TRUE")
attrs_lvds_o = Attrs(IOSTANDARD="LVDS")
attrs_33 = Attrs(IOSTANDARD="LVCMOS33", SLEW="SLOW", DRIVE="4")
attrs_33_pu = Attrs(IOSTANDARD="LVCMOS33", SLEW="SLOW", DRIVE="4", PULLUP="TRUE")
attrs_18_slow = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4")
attrs_18_slow_pu = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4", PULLUP="TRUE")
attrs_18_fast = Attrs(IOSTANDARD="LVCMOS18", SLEW="FAST", DRIVE="4")
attrs_18_dci = Attrs(IOSTANDARD="HSTL_I_DCI_18", SLEW="FAST")

def mgt(n, txp, txn, rxp, rxn):
    return Resource("mgt", n,
                    Subsignal("tx", DiffPairs(txp, txn, dir="o")),
                    Subsignal("rx", DiffPairs(rxp, rxn, dir="i")),
    )

class MediumPlatform(XilinxPlatform):
    device      = "xc7k160t"
    package     = "ffg676"
    speed       = "2"
    default_clk = "sync"
    resources = [
        Resource("clk156", 0, DiffPairs("D6", "D5", dir="i"), Clock(156.25e6)),
        # SFP0: bank 115 GTX inversions: R0, R3
        mgt(0, "M2", "M1", "N4", "N3"), # 1 in quad
        mgt(1, "K2", "K1", "L4", "L3"), # 2 in quad
        mgt(2, "P2", "P1", "R4", "R3"), # 0 in quad
        mgt(3, "H2", "H1", "J4", "J3"), # 3 in quad
        # SFP1: bank 116 GTX inversions: R2, T3
        mgt(4, "D2", "D1", "E4", "E3"), # 1 in quad
        mgt(5, "B2", "B1", "C4", "C3"), # 2 in quad
        mgt(6, "F2", "F1", "G4", "G3"), # 0 in quad
        mgt(7, "A4", "A3", "B6", "B5"), # 3 in quad
        Resource("led", 0, Pins("A12 A13 B14 A14", dir="o"), attrs_33),
        Resource("ufl", 0, Pins("AB16", dir="o"), attrs_18_dci),
        Resource("ufl", 1, Pins("AB20", dir="o"), attrs_18_dci),
        Resource("ufl", 2, Pins("N21",  dir="o"), attrs_18_fast),
        Resource("osc_tune", 0, Pins("B12",  dir="o"), attrs_33),
        Resource("qsfp", 0,
                 Subsignal("sda", Pins("B9", dir="io")),
                 Subsignal("scl", Pins("A9", dir="o")),
                 Subsignal("sel", Pins("B10 A10", dir="o")),
                 Subsignal("resetn", Pins("B11", dir="o")),
                 attrs_33,
        ),
        Resource("flash", 0,
                 Subsignal("cs", Pins("C23", dir="o")),
                 Subsignal("cipo", Pins("A25", dir="i")),
                 Subsignal("copi", Pins("B24", dir="o")),
                 attrs_18_slow_pu,
        ),
    ]
    mgt_invert_tx = [7]
    mgt_invert_rx = [0,3,6]
    connectors_hp = [0,1,4]
    connectors = [
        Connector(
            "exp", 0,
            "U2 U1 V1 V2 W1 Y1 AA2 AA3 AB1 AC1 AD1 AE1 AE2 AE3 AF2 AF3 AF4 AF5 AE5 AE6"),
        Connector(
            "exp", 1,
            "AC14 AD14 AE15 AD15 AB17 AC17 AD18 AC18 AF14 AF15 AE16 AD16 AD19 AC19 AF17 AE17 AF18 AE18 AF19 AF20"),
        Connector(
            "exp", 2,
            "AC22 AB22 AE25 AD25 AE26 AD26 AC24 AC23 AC26 AB26 Y25 Y26 W25 W26 V26 U26 U25 U24 W24 W23"),
        Connector(
            "exp", 3,
            "J24 J25 J26 H26 H23 H24 F24 G24 G26 G25 F25 E26 D26 C26 A24 A23 A22 B22 B20 A20"),
        Connector(
            "exp", 4,
            "U7 V6 W6 W5 Y6 Y5 AB4 AA4 AD6 AD5 AC3 AC4 AD4 AD3 AB2 AC2 W4 V4 W3 V3"),
    ]

    constraints = ""

    def create_pblock(self, name, region):
        self.constraints += r"""
        create_pblock {pblock}
        resize_pblock [get_pblocks {pblock}] -add {{CLOCKREGION_{cr}:CLOCKREGION_{cr}}}\n
        """.format(pblock="pblock_"+name, cr=region)

    def add_to_pblock(self, name, cells):
        self.constraints += "add_cells_to_pblock [get_pblocks {pblock}] [get_cells -quiet [list {cells}]]\n".format(pblock="pblock_"+name, cells=cells)

    def toolchain_prepare(self, fragment, name, **kwargs):
        overrides = {
            #"set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]",
            #"script_after_bitstream":
            #    "write_cfgmem -force -format bin -interface spix4 -size 16 "
            # "-loadbit \"up 0x0 {name}.bit\" -file {name}.bin".format(name=name),
            "add_constraints": "set_property INTERNAL_VREF 0.750 [get_iobanks 14]\n"
            "create_clock -name clk156_0__i -period 6.4 [get_nets clk156_0__p]\n"
            "set_property INTERNAL_VREF 0.9 [get_iobanks 32]\n"
            "set_property INTERNAL_VREF 0.9 [get_iobanks 15]\n"
            "set_property CONFIG_VOLTAGE 1.8 [current_design]\n"
            "set_property CFGBVS GND [current_design]\n"
            "set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]\n"
            "set_operating_conditions -ambient_temp 55.0\n" + self.constraints,
            "script_after_synth": r"""
            foreach cell [get_cells -quiet -hier -filter {hi.false_path != ""}] {
                set_false_path -from [get_property hi.false_path $cell] -to $cell
            }
            """
        }
        return super().toolchain_prepare(fragment, name, **overrides, **kwargs)
