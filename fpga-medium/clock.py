# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import sys
from amaranth import *
from XC7.clockbuffers import bufh_instance, bufg_instance
from XC7.SERDES import idelayctrl_instance

class ClockGen(Elaboratable):
    def __init__(self, cin):
        self.idelayready = Signal()
        self.pll_locked = Signal()
        self.reset = Signal(reset=1)
        self.cin = cin # 156.25 MHz
    def elaborate(self, platform):
        m = Module()

        c200i = Signal()
        c200ib = Signal()
        c250i = Signal()
        c250ib = Signal()
        c125i = Signal()
        c125ib = Signal()
        c625i = Signal()
        c625ib = Signal()
        c312i = Signal()
        c156i = Signal()
        cunusedi = Signal()

        pll_fb = Signal()
        pll_fbb = Signal()

        m.domains.clk156i = ClockDomain(reset_less=True) # 156.25 MHz via BUFH
        m.submodules.bufh_156i = bufh_instance(i=self.cin, o="clk156i")

        m.submodules.mmcm_sys = Instance(
            "MMCME2_BASE",
            a_LOC="MMCME2_ADV_X0Y4",
            p_STARTUP_WAIT="TRUE",
            # VCO @ 1250 MHz
            p_REF_JITTER1=0.01, p_CLKIN1_PERIOD=6.4,
            p_CLKFBOUT_MULT_F=8, p_DIVCLK_DIVIDE=1,
            i_CLKIN1=ClockSignal("clk156i"),
            i_CLKFBIN=pll_fb,
            i_RST=C(0),
            i_PWRDWN=C(0),
            # outputs
            o_CLKFBOUT=pll_fb,
            o_CLKFBOUTB=pll_fbb,
            p_CLKOUT0_DIVIDE_F=6.25, p_CLKOUT0_PHASE=0.0,
            p_CLKOUT1_DIVIDE=5, p_CLKOUT1_PHASE=0.0,
            p_CLKOUT2_DIVIDE=10, p_CLKOUT2_PHASE=0.0,
            p_CLKOUT3_DIVIDE=2, p_CLKOUT3_PHASE=0.0,
            p_CLKOUT4_DIVIDE=4, p_CLKOUT4_PHASE=0.0,
            p_CLKOUT5_DIVIDE=8, p_CLKOUT5_PHASE=0.0,
            o_CLKOUT0 = c200i,
            o_CLKOUT1 = c250i,
            o_CLKOUT2 = c125i,
            o_CLKOUT3 = c625i,
            o_CLKOUT0B = c200ib,
            o_CLKOUT1B = c250ib,
            o_CLKOUT2B = c125ib,
            o_CLKOUT3B = c625ib,
            o_CLKOUT4 = c312i,
            o_CLKOUT5 = c156i,
            o_CLKOUT6 = cunusedi,
            o_LOCKED=self.pll_locked,
        )
        m.submodules.bufg_200 = bufg_instance(i=c200i, o="clk200")
        m.submodules.bufg_250 = bufg_instance(i=c250i, o="clk250")
        m.submodules.bufg_125 = bufg_instance(i=c125i, o="sync")
        m.submodules.bufg_625 = bufg_instance(i=c625i, o="clk625")
        m.submodules.bufg_312 = bufg_instance(i=c312i, o="clk312")
        m.submodules.bufg_156 = bufg_instance(i=c156i, o="clk156")

        reset_counter = Signal(8, reset=255)
        with m.If(reset_counter != 0):
            m.d.sync += reset_counter.eq(reset_counter - 1)

        m.d.sync += self.reset.eq(reset_counter != 0)

        m.submodules.idelayctrl = idelayctrl_instance(clk="clk200", reset=self.reset,
                                                      ready=self.idelayready)

        return m
