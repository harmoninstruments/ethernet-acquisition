#!/usr/bin/env python3
# Copyright 2019 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import sys
sys.path.append("../fpga/harmon-instruments-open-hdl/")
import time
from amaranth import *
from clock import ClockGen
from board import MediumPlatform
from XC7.XADC import XADC
from XC7.DNA import DNA
from XC7.clockbuffers import *
from XC7.GTXE2_COMMON import *
from XC7.GTXE2_CHANNEL import *
from Ethernet.BlockSync64b66b import *
from Ethernet.Scrambler10GBASER import Descrambler
from Ethernet.XC7_40GBASER_TX import *
from Ethernet.XC7_10GBASER_TX import *
from Ethernet.TX_40GBASER import *
from Ethernet.TXbuf_32 import TXbuf_32
from Ethernet.RXbuf_32 import RXbuf_32
from Ethernet.RX_10GBASER import RX_10GBASER
from AXI.Stream import AXIStream
from Memory.FIFO import FIFO
from RiscV.RiscV import RiscV
from SPI.flash import SPI_flash_XC7
from XC7.STARTUPE2 import STARTUPE2

from amaranth.build import *
def get_ltc2208(port):
    return [
        Resource(
            "ltc2208", port,
            Subsignal("d", Pins("1 2 3 4 5 6 7 9 10 11 12 13 14 15 16 17", dir="i", conn=("exp", port))),
            Subsignal("of", Pins("18", dir="i", conn=("exp", port))),
            Subsignal("clock", Pins("8", dir="i", conn=("exp", port)), Clock(130e6)),
            Attrs(IOSTANDARD="HSTL_I_18"),
        ),
    ]

import struct
our_mac = bytes((0x02,0xDA,0xAD,0xBE,0xEF,0x00))
our_ip = bytes((10,42,0,10))

# https://tools.ietf.org/html/rfc1071
def csum16(data):
    csum = 0
    for i in range(len(data)//2):
        csum += struct.unpack(">H",data[2*i:2*(i+1)])[0]
    while (csum >> 16) > 0:
        csum = (csum & 0xFFFF) + (csum >> 16)
    return csum ^ 0xFFFF

def ipv4(dest_mac, dest_ip, proto, data):
    rv = bytearray()
    rv += dest_mac + our_mac
    rv += bytes((0x08, 0x00)) # ethertype
    rv += bytes((0x45, 0x00)) # ip hdr len
    rv += struct.pack(">H", len(data) + 20) # total length
    rv += bytes((0x00, 0x00, 0x00, 0x00)) # id, frag off
    rv += bytes((255, proto)) # ttl, protocol
    rv += bytes((0x00, 0x00)) # checksum
    rv += our_ip
    rv += dest_ip
    ip_hdr = rv[14:]
    csum = csum16(ip_hdr)
    rv[14+11] = csum & 0xFF
    rv[14+10] = csum >> 8
    assert len(rv) == 34
    rv += data
    return rv

def udp(dest_mac, dest_ip, source_port, dest_port, data):
    hdr = struct.pack(">HHHH", source_port, dest_port, len(data) + 8, 0)
    return ipv4(dest_mac, dest_ip, 17, hdr+data)

pkt10 = udp(
    dest_mac = bytes((0xFF,0xFF,0xFF,0xFF,0xFF,0xFF)),
    dest_ip = bytes((10,42,0,1)),
    source_port = 40000,
    dest_port = 40002,
    data = b"hello world10\n" + bytes(8916)
)

pkt = udp(
    dest_mac = bytes((0xFF,0xFF,0xFF,0xFF,0xFF,0xFF)),
    dest_ip = bytes((10,42,1,1)),
    source_port = 40000,
    dest_port = 40002,
    data = b"hello world40\n" + bytes(7000)
)

class Medium(Elaboratable):
    def elaborate(self, platform):
        gt_pins = []
        for i in range(8):
            gt_pins += [platform.request("mgt", i, dir={"tx":'-', "rx":'-'})]
        #platform.create_pblock("X0Y0", "X0Y0")
        clk156_pin = platform.request("clk156", dir="-")
        m = Module()
        m.domains.sync = ClockDomain(reset_less=True) # 156.25 MHz via BUFG
        m.domains.clk200 = ClockDomain(reset_less=True)
        m.domains.clk250 = ClockDomain(reset_less=True)
        m.domains.clk625 = ClockDomain(reset_less=True)
        m.domains.clk312 = ClockDomain(reset_less=True)
        m.domains.clk156 = ClockDomain(reset_less=True)
        m.domains.txclk = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFH
        m.domains.txclkg = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFHCE
        m.domains.rxclk0 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk0g = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFHCE
        m.domains.txclk4 = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFH
        m.domains.txclk4g = ClockDomain(reset_less=True) # 322 MHz via GTX TX via BUFHCE
        m.domains.rxclk4 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk5 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk6 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        m.domains.rxclk7 = ClockDomain(reset_less=True) # 322 MHz via GTX RX via BUFH
        clk156_ibuf = Signal()
        m.submodules.ibuf_gt = Instance("IBUFDS_GTE2",
                                        i_I=clk156_pin.p, i_IB=clk156_pin.n,
                                        i_CEB=C(0),
                                        o_O=clk156_ibuf,
                                        o_ODIV2=Signal()
        )
        count = Signal(32)
        m.d.sync += count.eq(count + 1)

        qsfp = platform.request("qsfp")

        reset = Signal(reset=1)
        with m.If(count[26]):
            m.d.sync += reset.eq(0)

        m.d.sync += qsfp.resetn.eq(~reset)
        m.d.comb += qsfp.sel.eq(0)

        clockgen = m.submodules.clockgen = ClockGen(clk156_ibuf)

        rdata_rx = Signal(32)
        cpu = RiscV(reset=clockgen.reset, programfile='../firmware/firmware.bin', rdata_or=rdata_rx)
        cpu.add_ro_reg(0x10000000, count)
        cpu.add_ro_reg(0x10000004, int(time.time()))
        cpu.add_ro_reg(0x10000008, Cat(clockgen.idelayready, clockgen.pll_locked))
        m.submodules.dna = DNA(cpu, 0x1000000C)
        startup = m.submodules.startup = STARTUPE2()
        m.submodules.flash = SPI_flash_XC7(
            pins=platform.request('flash'), sck=startup.usrcclk, pio=cpu, addr=0x10000010)

        m.submodules.xadc = XADC(cpu, 0x10000100)

        ucled = Signal()
        cpu.add_wo_reg(0x10000004, ucled)

        ns500 = Signal()
        with m.If(count[10]):
            m.d.sync += ns500.eq(1)

        gtc0 = m.submodules.gtxe2_common0 = GTXE2_COMMON(refclk0=clk156_ibuf, qpll_reset=reset & ns500)

        gtx0 = m.submodules.gtx0 = GTXE2_CHANNEL(
            pins=gt_pins[0], qpll=gtc0,
            txusrclk='txclk', rxusrclk='rxclk0',
            invert_tx = 0 in platform.mgt_invert_tx, invert_rx = 0 in platform.mgt_invert_rx,
            drp_clock = 'sync',
            pio = cpu,
            addr = 0x10000014,
        )

        m.submodules.bufh_rx0 = bufh_instance(i=gtx0.rxclk, o="rxclk0", loc = "BUFHCE_X1Y38")
        rx0ce = Signal()
        m.submodules.bufhce_rx0 = bufhce_instance(
            i=gtx0.rxclk, ce=rx0ce, o='rxclk0g', loc = "BUFHCE_X1Y39")

        tx10c = m.submodules.tx10c = XC7_10GBASER_TX_Common(clk='txclk')
        m.submodules.bufh_tx = bufh_instance(i=gtx0.txclk, o="txclk", loc = "BUFHCE_X1Y36")
        m.submodules.bufhce_tx = bufhce_instance(i=gtx0.txclk, ce=tx10c.ce, o='txclkg',
                                                 loc = "BUFHCE_X1Y37")

        m.submodules.txbuf10 = txbuf10 = TXbuf_32(
            wv=cpu.get_wvalid(0x20000000,lsb=22),
            wbe=cpu.wmask,
            wdata=cpu.wdata,
            waddr=cpu.waddr[:11],
            wrclk = 'sync',
            rdclk = 'txclkg',
        )
        cpu.add_ro_reg(0x20000000, txbuf10.busy, lsb=14)
        m.d.comb += txbuf10.ostream.ready.eq(1)

        valid_prev = Signal()
        m.d.txclkg += valid_prev.eq(txbuf10.ostream.valid)
        start3 = Signal()
        m.d.comb += start3.eq(txbuf10.ostream.valid & ~valid_prev)

        tx10 = m.submodules.tx10 = XC7_10GBASER_TX(
            idata=txbuf10.ostream.data,#data2,
            istart=start3,
            ilast=txbuf10.ostream.last & txbuf10.ostream.valid,#last2,
            isize=txbuf10.ostream.size,#size2,
            common=tx10c,
            clk='txclkg',
            gtx=gtx0)

        # RX 10
        reset_rx = Signal(reset=1)
        with m.If(count[30]):
            m.d.sync += reset_rx.eq(0)

        m.d.comb += gtx0.gtrxreset.eq(reset & reset_rx)

        rx0_header_err = Signal()
        rx0_header_control = Signal()
        rx0_header_valid = Signal()

        bs = m.submodules.blocksync = BlockSync64b66b(
            header_err = rx0_header_err,
            header_valid = rx0_header_valid,
            slip = gtx0.rxgearboxslip,
            clk='rxclk0')

        rxdata_reversed = Signal(32)
        m.d.rxclk0 += [
            rx0ce.eq(gtx0.rxdatavalid),
            rx0_header_valid.eq(gtx0.rxheadervalid),
        ]
        with m.If(gtx0.rxheadervalid):
            m.d.rxclk0 += [
                rx0_header_control.eq(gtx0.rxheader[1]),
                rx0_header_err.eq(gtx0.rxheader[0] == gtx0.rxheader[1]),
            ]
        with m.If(gtx0.rxdatavalid):
            m.d.rxclk0 += rxdata_reversed.eq(gtx0.rxdata[:32][::-1])

        descramble = m.submodules.descramble = Descrambler(i=rxdata_reversed, clk='rxclk0g')
        hdrvalid_1 = Signal()
        hdrcontrol_1 = Signal()
        hdrerr_1 = Signal()
        m.d.rxclk0g += [
            hdrvalid_1.eq(rx0_header_valid),
            hdrcontrol_1.eq(rx0_header_control),
            hdrerr_1.eq(rx0_header_err),
        ]

        rx10 = m.submodules.rx10 = RX_10GBASER(
            idata=descramble.o,
            icontrol=hdrcontrol_1,
            ierror=hdrerr_1,
            ilast=~hdrvalid_1,
            clk='rxclk0g'
        )

        rxb10 = m.submodules.rxbuf32 = RXbuf_32(
            din=rx10.o,
            rvalid=cpu.rvalid & (cpu.dbus_addr[28:32] == 0x3),
            raddr=cpu.dbus_addr[2:14],
            wvalid=cpu.wvalid & (cpu.waddr[26:30] == 0x3),
            wdata=cpu.wdata,
            clk_in='rxclk0g',
            primitive = 'XC7',
            clockname_in = 'gtx0_rxclk',
        )

        m.d.comb += rdata_rx.eq(rxb10.rdata)

        ufl0 = platform.request("ufl", 0)
        ufl1 = platform.request("ufl", 1)
        ufl2 = platform.request("ufl", 2)
        m.d.comb += ufl0.eq(gtx0.rxheadervalid)
        m.d.comb += ufl1.eq(rx10.o.valid)
        m.d.comb += ufl2.eq(bs.header_fail)

        count_rxclk = Signal(27)
        m.d.rxclk0g += count_rxclk.eq(count_rxclk + 1)

        m.domains.adclk1 = ClockDomain(reset_less=True)
        m.domains.adclk1io = ClockDomain(reset_less=True)
        adc1 = platform.request("ltc2208", 1, xdr={"d":1, "of":1}, dir={"clock":"i"})
        m.submodules['bufr_adc1'] = bufr_instance(i=adc1.clock, o="adclk1")
        m.submodules['bufio_adc1'] = bufio_instance(i=adc1.clock, o="adclk1io")
        count_adc = Signal(27)
        adc1_in = Signal(signed(16))
        m.d.comb += [
            adc1.d.i_clk.eq(ClockSignal("adclk1io")),
            adc1.of.i_clk.eq(ClockSignal("adclk1io")),
        ]
        m.d.adclk1 += [
            count_adc.eq(count_adc + 1),
            adc1_in[0].eq(adc1.d.i[0]),
            adc1_in[1:].eq(Mux(adc1.d.i[0], ~adc1.d.i[1:], adc1.d.i[1:])),
        ]

        led = platform.request("led", 0)
        m.d.comb += led.eq(Cat(bs.lock, count_rxclk[-1], ucled, (adc1_in > 64)))

        # oscillator PWM tuning voltage, 50% for now
        osc_tune = platform.request("osc_tune")
        m.d.sync += osc_tune.eq(count[10])

        platform.constraints += "create_clock -period 3.103 [get_nets {gtx0_txclk}]\n"
        platform.constraints += "create_clock -period 3.103 [get_nets {gtx0_rxclk}]\n"

        """
        gtc4 = m.submodules.gtxe2_common4 = GTXE2_COMMON(refclk0=clk156_ibuf, qpll_reset=reset & ns500)
        gtx = []
        for i in range(4,8):
            inst = m.submodules['gtx{}'.format(i)] = GTXE2_CHANNEL(
                pins=gt_pins[i], qpll=gtc4,
                txusrclk='txclk4', rxusrclk='rxclk{}'.format(i),
                invert_tx = i in platform.mgt_invert_tx, invert_rx = i in platform.mgt_invert_rx)
            gtx.append(inst)
            m.submodules['bufh_rx{}'.format(i)] = bufh_instance(i=inst.rxclk, o="rxclk{}".format(i))
        m.submodules.bufh_tx4 = bufh_instance(i=gtx[0].txclk, o="txclk4")
        txce40 = Signal()
        m.submodules.bufhce_tx4 = bufhce_instance(i=gtx[0].txclk, ce=txce40, o="txclk4g")

        qinit = []
        for i in range((len(pkt)+15)//16):
            b = pkt[16*i:16*(i+1)]
            d = 0
            for a in b[::-1]:
                d <<= 8
                d |= a
            qinit.append(d)

        qram = Memory(width=128, depth=512, init=qinit)
        qrdport = m.submodules.qrdport = qram.read_port(domain="txclk4g", transparent=False)
        qra = Signal(9)
        tx40_ready = Signal()
        m.d.comb += [
            qrdport.en.eq(1),
            qrdport.addr.eq(qra),
        ]

        start1 = Signal()
        last1 = Signal()
        size1 = Signal(4)
        trig2 = Signal()
        counter = Signal(32)

        m.d.txclk4g += [
            qra.eq(qra + 1),
            start1.eq(qra == 0),
            last1.eq(qra == 500),
            size1.eq(size1 + (qra==510)),
            trig2.eq(qra == 6),
            counter.eq(counter + start1),
        ]

        tx40mac = m.submodules.tx40mac = TX_40GBASER(
            idata=Mux(trig2, counter, qrdport.data),
            istart=start1, ilast=last1, isize=size1, clk='txclk4g')

        txstream = AXIStream('txdata40', [('data', 128), ('control', 2)])

        m.d.comb += txstream.valid.eq(1)
        m.d.comb += txstream.data.eq(tx40mac.odata)
        m.d.comb += txstream.control.eq(tx40mac.ocontrol)

        txfifo = m.submodules.txfifo40 = FIFO(
            i=txstream, depth=16, iclk='txclk4g', oclk='txclk4', synchronous=True)
        m.d.comb += txfifo.o.ready.eq(tx40_ready)
        m.d.txclk4 += txce40.eq(txfifo.count_o < 8)

        tx40 = m.submodules.tx40 = XC7_40GBASER_TX(
            idata=txfifo.o.data, icontrol=txfifo.o.control, clk='txclk4', gtx=gtx)

        m.d.comb += tx40_ready.eq(tx40.iready)

        for i in range(4):
            m.d.comb += gtx[i].gtrxreset.eq(ns500 & reset_rx)

        platform.constraints += "create_clock -period 3.103 [get_nets {gtx4_txclk}]\n"
        for i in range(4,8):
            platform.constraints += "create_clock -period 3.103 [get_nets {{gtx{}_rxclk}}]\n".format(i)
        """
        for c in platform.iter_clock_constraints():
            print(c)

        m.submodules.cpu = cpu

        return m

if __name__ == "__main__":
    p = MediumPlatform()
    p.add_resources(get_ltc2208(1))
    p.build(Medium(), do_program=False)
