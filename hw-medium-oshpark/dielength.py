#!/usr/bin/env python3

# kicad file is first arg, csv of lengths is 2nd
# report_csv in Vivado TCL, clean up with spreadsheet
import sys, math
from pcbnew import *

pcb = LoadBoard(sys.argv[1])

lengths = {}

with open(sys.argv[2]) as f:
    for l in f:
        d = l.split(',')
        try:
            lengths[d[0]] = (float(d[1]) + float(d[2])) * 0.5
        except:
            pass

mod = pcb.FindModuleByReference('U8')
pads = mod.Pads()

for pad in pads:
    name = pad.GetPadName()
    if name in lengths:
        pad.SetPadToDieLength(FromMM(lengths[name]))

pcb.Save("./mod.kicad_pcb")
